﻿using Newtonsoft.Json;
using System.Data.Entity;
using System.Linq;
using XFilter.DataModel.Models;


namespace XFilter.DataModel.DTO.TableObjects
{
    /// <summary>
    /// The agent server relay alchemy custom feature configuration class.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public sealed class AgentAlchemyConfigDTO : IDataTransferObject
    {
        //--------------------------------------------------------------------------

        #region Properties

        /// <summary>
        /// The maximum item plus value (if you set it to 10, player will get message saying
        /// that he can not plus this item any futher).
        /// </summary>
        [JsonProperty(PropertyName = "Maximum plus limit enabled/disabled")]
        public bool? MaxPlusLimitEnabled
        { get; private set; }


        /// <summary>
        /// Gets maximum item plus value.
        /// </summary>
        [JsonProperty(PropertyName = "Maximum plus item levle")]
        public int? MaxPlusItemLevel
        { get; private set; }

        /// <summary>
        /// Gets or sets 'Maximum plus level is {max_item_plus}'
        /// </summary>
        [JsonProperty(PropertyName = "Maximum item plus limit reached msg")]
        public string MaxPlusLimitMsg
        { get; private set; }

        /// <summary>
        /// Level at which plus success/failure messages will appear (if enabled).
        /// </summary>
        [JsonProperty(PropertyName = "Item plus success/failure item min plus")]
        public int? PlusNoticeItemLevel
        { get; private set; }

        /// <summary>
        /// Defines if item plus success message should be broadcasted to all connected players.
        /// </summary>
        [JsonProperty(PropertyName = "Item plus success notice msg enabled/disabled")]
        public bool? PlusSuccessNoticeEnabled
        { get; private set; }

        /// <summary>
        /// 'Player {charname} succeeded plussing {item_name} to {new_item_level}'
        /// </summary>
        [JsonProperty(PropertyName = "Item plus success message")]
        public string PlusSuccessNoticeMsg
        { get; private set; }

        /// <summary>
        /// Defines if item plus failure message should be broadcasted to all connected players.
        /// </summary>
        [JsonProperty(PropertyName = "Item plus failure message enabled/disabled")]
        public bool? PlusFailureNoticeEnabled
        { get; private set; }


        /// <summary>
        /// 'Player {charname} failed to plus {item_name} to {new_item_level}'
        /// </summary>
        [JsonProperty(PropertyName = "Item plus failure message")]
        public string PlusFailureNoticeMsg
        { get; private set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        /// <summary>
        /// The alchemy configuration data transfer object constructor (from EF table record).
        /// </summary>
        /// <param name="cfg"></param>
        public AgentAlchemyConfigDTO(C_AlchemyConfig cfg)
        {
            this.MaxPlusLimitEnabled = cfg.MaxPlusLimitEnabled;
            this.MaxPlusItemLevel = cfg.MaxPlusItemLevel;
            this.MaxPlusLimitMsg = cfg.MaxPlusLimitMsg;
            this.PlusNoticeItemLevel = cfg.PlusNoticeItemLevel;
            this.PlusSuccessNoticeEnabled = cfg.PlusSuccessNoticeEnabled;
            this.PlusSuccessNoticeMsg = cfg.PlusSuccessNoticeMsg;
            this.PlusFailureNoticeEnabled = cfg.PlusFailureNoticeEnabled;
            this.PlusFailureNoticeMsg = cfg.PlusFailureNoticeMsg;
        }

        /// <summary>
        /// Requred for proper JSON deserialization.
        /// </summary>
        public AgentAlchemyConfigDTO()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        /// <summary>
        /// Saves settings into database.
        /// Does <b>not</b> flush. Flush must be done from AgentConfigEntity.
        /// </summary>
        /// <param name="context">The database context.</param>
        /// <param name="serverId">The referenced server id.</param>
        public void Save(DbContext context, int serverId)
        {
            var entities = context as XServiceDBEntities;
            var rec = entities.C_AlchemyConfig.First(agent => agent.ID == serverId);
            rec.MaxPlusLimitEnabled = this.MaxPlusLimitEnabled;
            rec.MaxPlusItemLevel = this.MaxPlusItemLevel;
            rec.MaxPlusLimitMsg = this.MaxPlusLimitMsg;
            rec.PlusNoticeItemLevel = this.PlusNoticeItemLevel;
            rec.PlusSuccessNoticeEnabled = this.PlusSuccessNoticeEnabled;
            rec.PlusSuccessNoticeMsg = this.PlusSuccessNoticeMsg;
            rec.PlusFailureNoticeEnabled = this.PlusFailureNoticeEnabled;
            rec.PlusFailureNoticeMsg = this.PlusFailureNoticeMsg;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}

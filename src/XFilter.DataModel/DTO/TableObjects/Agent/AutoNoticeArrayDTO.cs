﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using XFilter.DataModel.Models;

namespace XFilter.DataModel.DTO.TableObjects
{
    /// <summary>
    /// The agent relay server auto notice configuration data transfer object class.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public sealed class AutoNoticeArrayDTO : IDataTransferObject
    {
        //--------------------------------------------------------------------------

        #region Public properties
        
        /// <summary>
        /// The auto notice definitions (text/delay).
        /// </summary>
        [JsonProperty(PropertyName = "Auto notices")]
        public ICollection<AutoNoticeDTO> Notices
        { get;  private set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        /// <summary>
        /// The AutoNoticeArrayDTO constructor.
        /// </summary>
        public AutoNoticeArrayDTO()
        {

        }

        /// <summary>
        /// The AutoNoticeArrayDTO constructor (from db records).
        /// </summary>
        /// <param name="notices">Notice data rows.</param>
        public AutoNoticeArrayDTO(IQueryable<C_AutoNotice> notices)
        {
            this.Notices = new List<AutoNoticeDTO>();
            foreach (var notice in notices)
            {
                this.Notices.Add(
                    new AutoNoticeDTO(notice.Interval, notice.Text)
                    );
            }
        }

        /// <summary>
        /// Saves settings into database.
        /// Does <b>not</b> flush. Flush must be done from AgentConfigEntity.
        /// </summary>
        /// <param name="context">The database context.</param>
        /// <param name="serverId">The referenced server id.</param>
        public void Save(DbContext context, int serverId)
        {
            var entities = context as XServiceDBEntities;
            var records = entities.C_AutoNotice.Where(rec => rec.RefServerID == serverId);

            entities.C_AutoNotice.RemoveRange(records);

            foreach (var notice in this.Notices)
            {
                entities.C_AutoNotice.Add(new C_AutoNotice()
                {
                    RefServerID = serverId,
                    Interval = notice.Interval,
                    Text = notice.Text
                });
            }
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}

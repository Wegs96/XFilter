﻿using Newtonsoft.Json;
using System.Data.Entity;
using System.Linq;
using XFilter.DataModel.Models;

namespace XFilter.DataModel.DTO.TableObjects
{
    /// <summary>
    /// Farm-specific configuration only.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public sealed class FarmConfigDTO : IDataTransferObject
    {
        //--------------------------------------------------------------------------

        #region Public properties

        /// <summary>
        /// The secret API key.
        /// </summary>
        [JsonProperty(PropertyName = "API key")]
        public string ApiKey
        { get; private set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        /// <summary>
        /// The FarmConfigDTO constructor (from RF record).
        /// </summary>
        /// <param name="cfg"></param>
        public FarmConfigDTO(C_FarmServers cfg)
        {
            this.ApiKey = cfg.ApiKey;
        }

        /// <summary>
        /// Requred for proper JSON deserialization.
        /// </summary>
        public FarmConfigDTO()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        /// <summary>
        /// Saves settings into database.
        /// Does <b>not</b> flush. Flush must be done from AgentConfigEntity.
        /// </summary>
        /// <param name="context">The database context.</param>
        /// <param name="serverId">The referenced server id.</param>
        public void Save(DbContext context, int serverId)
        {
            var entities = context as XServiceDBEntities;
            var farmRec = entities.C_FarmServers.First(farm => farm.RefServerID == serverId);
            farmRec.ApiKey = this.ApiKey;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}

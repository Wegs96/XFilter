﻿namespace XFilter.DataModel.Protocol.Agent
{
    using NLog;
    using System;
    using XFilter.Shared.Network;


    /// <summary>
    /// S->C SERVER_AGENT_LOGOUT_CANCEL_RESPONSE = 0xB006
    /// TESTME
    /// </summary>
    public class AuthResponseEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties
        
        public PacketResultCode Code
        { get; set; }

        public AuthErrorCode ErrorCode
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        /// <summary>
        /// Success ctor.
        /// </summary>
        /// <param name="errorCode"></param>
        public AuthResponseEntity()
        {
            this.Code = PacketResultCode.Success;
        }

         /// <summary>
         /// Error ctor.
         /// </summary>
         /// <param name="errorCode"></param>
        public AuthResponseEntity(AuthErrorCode errorCode)
        {
            this.Code = PacketResultCode.Error;
            this.ErrorCode = errorCode;
        }



        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.Code = packet.ReadEnum<PacketResultCode>();

            if (this.Code == PacketResultCode.Error)
                this.ErrorCode = packet.ReadEnum<AuthErrorCode>();

            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.SERVER_AGENT_AUTH_RESPONSE);

            packet.WriteValue<byte>((byte)this.Code);
            if (this.Code == PacketResultCode.Error)
                packet.WriteEnum<AuthErrorCode>(this.ErrorCode);

            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}

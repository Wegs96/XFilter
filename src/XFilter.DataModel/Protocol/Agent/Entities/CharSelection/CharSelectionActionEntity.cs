﻿using NLog;
using System;
using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// C->S CLIENT_AGENT_CHARACTER_SELECTION_ACTION_REQUEST = 0x7007
    /// TESTME
    /// </summary>
    public class CharSelectionActionEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties

        public CharSelectionAction Action
        { get; set; }

        public string Name
        { get; set; }

        public uint RefObjID
        { get; set; }
        
        public byte Scale
        { get; set; }
    
        public uint RefChestItemID
        { get; set; }

        public uint RefPantsItemID
        { get; set; }

        public uint RefBootsItemID
        { get; set; }

        public uint RefWeaponItemID
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public CharSelectionActionEntity(CharSelectionAction action, string name = null)
        {
            this.Action = action;
            if(this.Action == CharSelectionAction.Create)
                throw new ArgumentException("Use overloaded constructor for character creation action.");

            if (this.Action == CharSelectionAction.Delete ||
              this.Action == CharSelectionAction.CheckName ||
              this.Action == CharSelectionAction.Restore)
            {
                if (name == null)
                    throw new ArgumentNullException($"Name must be specified for action mode {this.Action}.");
                this.Name = name;
            }
        }

      
        public CharSelectionActionEntity(
            string name, uint refObjId, byte scale,
            uint refChestItemID, uint refPantsItemID,
            uint refBootsItemID, uint refWeaponItemID)
        {
            this.Action = CharSelectionAction.Create;
            this.Name = name;
            this.RefObjID = refObjId;
            this.Scale = scale;
            this.RefChestItemID = refChestItemID;
            this.RefPantsItemID = refPantsItemID;
            this.RefBootsItemID = refBootsItemID;
            this.RefWeaponItemID = refWeaponItemID;
        }

        public CharSelectionActionEntity()
        {

        }


        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.Action = packet.ReadEnum<CharSelectionAction>();

            if (this.Action == CharSelectionAction.Create)
            {
                this.Name = packet.ReadValue<string>();
                this.RefObjID = packet.ReadValue<uint>();
                this.Scale = packet.ReadValue<byte>();

                this.RefChestItemID = packet.ReadValue<uint>();
                this.RefPantsItemID = packet.ReadValue<uint>();
                this.RefBootsItemID = packet.ReadValue<uint>();
                this.RefWeaponItemID = packet.ReadValue<uint>();
            }

            if (this.Action == CharSelectionAction.Delete ||
               this.Action == CharSelectionAction.CheckName ||
               this.Action == CharSelectionAction.Restore)
            {
                this.Name = packet.ReadValue<string>();
            }

            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.CLIENT_AGENT_CHARACTER_SELECTION_ACTION_REQUEST);
            packet.WriteEnum<CharSelectionAction>(this.Action);

            if (this.Action == CharSelectionAction.Create)
            {
                packet.WriteValue<string>(this.Name);
                packet.WriteValue<uint>(this.RefObjID);
                packet.WriteValue<byte>(this.Scale);

                packet.WriteValue<uint>(this.RefChestItemID);
                packet.WriteValue<uint>(this.RefPantsItemID);
                packet.WriteValue<uint>(this.RefBootsItemID);
                packet.WriteValue<uint>(this.RefWeaponItemID);

            }


            if (this.Action == CharSelectionAction.Delete ||
                this.Action == CharSelectionAction.CheckName ||
                this.Action == CharSelectionAction.Restore)
            {
                packet.WriteValue<string>(this.Name);
            }

            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}

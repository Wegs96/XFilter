﻿using NLog;
using System.Collections.Generic;
using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// S->C SERVER_AGENT_CHARACTER_SELECTION_RESPONSE = B007
    /// TESTME
    /// </summary>
    public class CharSelectionCharInfo : IPacketInnerEntity
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties

        public uint RefObjID
        { get; set; }

        public string Name
        { get; set; }

        public byte Scale
        { get; set; }

        public byte CurLevel
        { get; set; }

        public ulong ExpOffset
        { get; set; }

        public ushort Strength
        { get; set; }

        public ushort Intellect
        { get; set; }

        public ushort RemainStatPoint
        { get; set; }

        public uint Health
        { get; set; }

        public uint Mana
        { get; set; }

        public bool Deleting
        { get; set; }

        /// <summary>
        /// Only if Deleting = true.
        /// </summary>
        public uint DeleteTime
        { get; set; }

        public CharSelectionGuildFlag GuildFlag
        { get; set; }

        public bool GuildRenameFlag
        { get; set; }

        /// <summary>
        /// If GuildRenameFlag = true.
        /// </summary>
        public string GuildName
        { get; set; }

        public CharSelectionAcademyFlag AcademyFlag
        { get; set; }

        public byte ItemCount
        { get; set; }

        //RefItemID, Plus
        public List<KeyValuePair<uint, byte>> Items
        { get; private set; }

        public List<KeyValuePair<uint, byte>> Avatars
        { get; private set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

      

        
        public CharSelectionCharInfo(
            uint refObjId, string name, byte scale, byte curLevel,
            ulong expOffset, ushort strength, ushort intellect,
            ushort remainStatPoints, uint health, uint mana,
            bool deleting, uint deleteTime, 
            CharSelectionGuildFlag guildFlag, bool guildRenameFlag,
            CharSelectionAcademyFlag academyFlag, 
            List<KeyValuePair<uint, byte>> items,
            List<KeyValuePair<uint, byte>> avatars


            )
        {
            this.RefObjID = refObjId;
            this.Name = name;
            this.Scale = scale;
            this.CurLevel = curLevel;
            this.ExpOffset = expOffset;
            this.Strength = strength;
            this.Intellect = intellect;
            this.RemainStatPoint = remainStatPoints;
            this.Health = health;
            this.Mana = mana;
            this.Deleting = deleting;
            this.DeleteTime = deleteTime;
            this.GuildFlag = guildFlag;
            this.GuildRenameFlag = guildRenameFlag;
            this.AcademyFlag = academyFlag;
            

            this.Items = items;
            this.Avatars = avatars;
        }

        public CharSelectionCharInfo()
        {

        }


        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.Items = new List<KeyValuePair<uint, byte>>();
            this.Avatars = new List<KeyValuePair<uint, byte>>();
            this.RefObjID = packet.ReadValue<uint>();
            this.Name = packet.ReadValue<string>();
            this.Scale = packet.ReadValue<byte>();
            this.CurLevel = packet.ReadValue<byte>();
            this.ExpOffset = packet.ReadValue<ulong>();
            this.Strength = packet.ReadValue<ushort>();
            this.Intellect = packet.ReadValue<ushort>();
            this.RemainStatPoint = packet.ReadValue<ushort>();
            this.Health = packet.ReadValue<uint>();
            this.Mana = packet.ReadValue<uint>();
            this.Deleting = packet.ReadValue<bool>();

            if (this.Deleting)
                this.DeleteTime = packet.ReadValue<uint>();


            this.GuildFlag = packet.ReadEnum<CharSelectionGuildFlag>();
            this.GuildRenameFlag = packet.ReadValue<bool>();

            this.AcademyFlag = packet.ReadEnum<CharSelectionAcademyFlag>();

            byte itemCount = packet.ReadValue<byte>();
            for (int i = 0; i < itemCount; i++)
            {
                var kvp = new KeyValuePair<uint, byte>(
                    packet.ReadValue<uint>(), packet.ReadValue<byte>());
                Items.Add(kvp);
            }

            itemCount = packet.ReadValue<byte>();
            for (int i = 0; i < itemCount; i++)
            {
                var kvp = new KeyValuePair<uint, byte>(
                    packet.ReadValue<uint>(), packet.ReadValue<byte>());
                Avatars.Add(kvp);
            }

            return true;
        }

        public bool AppendToPacket(XPacket packet)
        {
            packet.WriteValue<uint>(this.RefObjID);
            packet.WriteValue<string>(this.Name);
            packet.WriteValue<byte>(this.Scale);
            packet.WriteValue<byte>(this.CurLevel);
            packet.WriteValue<ulong>(this.ExpOffset);
            packet.WriteValue<ushort>(this.Strength);
            packet.WriteValue<ushort>(this.Intellect);
            packet.WriteValue<ushort>(this.RemainStatPoint);
            packet.WriteValue<uint>(this.Health);
            packet.WriteValue<uint>(this.Mana);
            packet.WriteValue<bool>(this.Deleting);

            if (this.Deleting)
                packet.WriteValue<uint>(this.DeleteTime);


            packet.WriteEnum<CharSelectionGuildFlag>(this.GuildFlag);
            packet.WriteValue<bool>(this.GuildRenameFlag);

            if (this.GuildRenameFlag)
                packet.WriteValue<string>(this.GuildName);

            packet.WriteEnum<CharSelectionAcademyFlag>(this.AcademyFlag);

            packet.WriteValue<byte>((byte)this.Items.Count);
            foreach(var item in this.Items)
            {
                //RefID, Plus.
                packet.WriteValue<uint>(item.Key);
                packet.WriteValue<byte>(item.Value);
            }

            packet.WriteValue<byte>((byte)this.Avatars.Count);
            foreach (var item in this.Avatars)
            {
                //RefID, Plus.
                packet.WriteValue<uint>(item.Key);
                packet.WriteValue<byte>(item.Value);
            }

            return true;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}

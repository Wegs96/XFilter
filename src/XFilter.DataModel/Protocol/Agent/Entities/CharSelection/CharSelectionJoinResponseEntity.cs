﻿using NLog;
using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// S->C  SERVER_AGENT_CHARACTER_SELECTION_JOIN_RESPONSE = 0xB001
    /// TESTME
    /// </summary>
    public class CharSelectionJoinResponseEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties

        public PacketResultCode Code
        { get; set; }

        public CharSelectionErrorCode ErrorCode
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors


        /// <summary>
        /// Constructor for error message.
        /// </summary>
        /// <param name="errorCode">Error code.</param>
        public CharSelectionJoinResponseEntity(CharSelectionErrorCode errorCode)
        {
            this.Code = PacketResultCode.Error;
            this.ErrorCode = errorCode;
        }

        /// <summary>
        /// Success ctor.
        /// </summary>
        public CharSelectionJoinResponseEntity()
        {
            this.Code = PacketResultCode.Success;
        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.Code = packet.ReadEnum<PacketResultCode>();

            if (this.Code == PacketResultCode.Error)
                this.ErrorCode = packet.ReadEnum<CharSelectionErrorCode>();
            
            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.SERVER_AGENT_CHARACTER_SELECTION_JOIN_RESPONSE);

            packet.WriteEnum<PacketResultCode>(this.Code);
            if (this.Code == PacketResultCode.Error)
                packet.WriteEnum<CharSelectionErrorCode>(this.ErrorCode);

            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}

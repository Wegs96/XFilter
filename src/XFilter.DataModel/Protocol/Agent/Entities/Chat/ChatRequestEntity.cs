﻿using NLog;
using System;
using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// C->S CLIENT_AGENT_CHAT_REQUEST = 0x7025
    /// TESTME
    /// </summary>
    public class ChatRequestEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties

        public ChatType ChatType
        { get; set; }

        public byte ChatIndex
        { get; set; }

        public string Receiver
        { get; set; }

        public string Message
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

      
     

        public ChatRequestEntity(ChatType chatType, byte chatIndex, string message, string receiver = null)
        {
            if (chatType == ChatType.Private && receiver == null)
                throw new ArgumentNullException("Receiver must be specified for private chat type.");

            this.ChatType = chatType;
            this.ChatIndex = chatIndex;
            this.Message = message;
            this.Receiver = receiver;
        }


        public ChatRequestEntity()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.ChatType = packet.ReadEnum<ChatType>();
            this.ChatIndex = packet.ReadValue<byte>();

            if (this.ChatType == ChatType.Private)
                this.Receiver = packet.ReadValue<string>();

            this.Message = packet.ReadValue<string>();

            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.CLIENT_AGENT_CHAT_REQUEST);

            packet.WriteEnum<ChatType>(this.ChatType);
            packet.WriteValue<byte>((byte)this.ChatIndex);
            
            if(this.ChatType == ChatType.Private && this.Receiver == null)
            {
                Logger.Error("Private chat must contain sender.");
                return null;
            }

            if (this.ChatType == ChatType.Private)
                packet.WriteValue<string>(this.Receiver);

            packet.WriteValue<string>(this.Message);

            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}

﻿using NLog;
using System.Collections.Generic;
using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// C->S CLIENT_AGENT_ENFORCEMENT_REINFORCE_REQUEST = 0x7150
    /// TESTME, INCOMPLETE (socket).
    /// </summary>
    public class AlchemyReinforceRequestEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties

        public AlchemyAction Action
        { get; private set; }

        public AlchemyFuseType FuseType
        { get; private set; }
  
        public byte ItemCount
        { get; private set; }

        private List<byte> _items;

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            _items = new List<byte>();

            this.Action = packet.ReadEnum<AlchemyAction>();

            if (this.Action == AlchemyAction.Cancel)
                return true;


            if(this.Action == AlchemyAction.Fusing)
            {
                this.FuseType = packet.ReadEnum<AlchemyFuseType>();

                //All except socket have same structure. 1 byte coutn and n 
                //elements of byte for slots.
                if (this.FuseType != AlchemyFuseType.Socket)
                {
                    this.ItemCount = packet.ReadValue<byte>();
                    for (int i = 0; i < this.ItemCount; i++)
                        _items.Add(packet.ReadValue<byte>());
                }
                else return false;
            }

            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.CLIENT_AGENT_ENFORCEMENT_REINFORCE_REQUEST);

            packet.WriteEnum<AlchemyAction>(this.Action);

            if (this.Action == AlchemyAction.Cancel)
            {
                Logger.Warn("Alchemy cancel action not yet implemented.");
                return packet;
            }

            packet.WriteEnum<AlchemyFuseType>(this.FuseType);

            if (this.Action == AlchemyAction.Fusing)
            {
                if (this.FuseType != AlchemyFuseType.Socket)
                {
                    packet.WriteValue<byte>((byte)_items.Count);
                    _items.ForEach(item => packet.WriteValue<byte>(item));
                }
            }

            return packet;
        }

        /// <summary>
        /// Main item is the item on which materials are used.
        /// </summary>
        /// <returns></returns>
        public byte GetMainItemSlot() => _items[0];

        /// <summary>
        /// Only material item slots are returned.
        /// </summary>
        /// <returns></returns>
        public byte[] GetMaterialItemSlots() => _items.GetRange(1, _items.Count - 1).ToArray();

        #endregion

        //--------------------------------------------------------------------------
    }
}

﻿using NLog;
using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// S->C SERVER_AGENT_FRPVP_UPDATE_RESPONSE = 0xB516
    /// TESTME
    /// </summary>
    public class FRPVPResponseEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties

        public PacketResultCode Code
        { get; set; }

        public uint UniqueID
        { get; set; }

        public FRPVPMode Mode
        { get; set; }

        public FRPVPErrorCode ErrorCode
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        /// <summary>
        /// Success message ctor.
        /// </summary>
        /// <param name="uniqueId">Unique nearby object id.</param>
        /// <param name="mode">PVP clan sign.</param>
        public FRPVPResponseEntity(uint uniqueId, FRPVPMode mode)
        {
            this.Code = PacketResultCode.Success;
            this.UniqueID = uniqueId;
            this.Mode = mode;
        }

        /// <summary>
        /// Error message ctor.
        /// </summary>
        /// <param name="errorCode">Error code.</param>
        public FRPVPResponseEntity(FRPVPErrorCode errorCode)
        {
            this.ErrorCode = errorCode;
        }

        public FRPVPResponseEntity()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.Code = packet.ReadEnum<PacketResultCode>();

            if (this.Code == PacketResultCode.Success)
            {
                this.UniqueID = packet.ReadValue<uint>();
                this.Mode = packet.ReadEnum<FRPVPMode>();
            }
            else
            {
                this.ErrorCode = packet.ReadEnum<FRPVPErrorCode>();
            }

            return true;
        }
        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.SERVER_AGENT_FRPVP_UPDATE_RESPONSE);

            packet.WriteEnum<PacketResultCode>(this.Code);

            if (this.Code == PacketResultCode.Success)
            {
                packet.WriteValue<uint>(this.UniqueID);
                packet.WriteEnum<FRPVPMode>(this.Mode);
            }
            else packet.WriteEnum<FRPVPErrorCode>(this.ErrorCode);

            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}

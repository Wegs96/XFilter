﻿using NLog;
using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// S->C SERVER_AGENT_GUILD_INFO_DATA = 0x3101
    /// TODO: Implement loading from db
    /// TESTME
    /// </summary>
    public class GuildMember : IPacketInnerEntity
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties

        public uint ID
        { get; set; }

        public string Name
        { get; set; }

        public GuildMemberClass MemberClass
        { get; set; }

        public byte Level
        { get; set; }

        public uint GPDonation
        { get; set; }

        public GuildPermission Permission
        { get; set; }

        public uint Contribution
        { get; set; }

        public uint GuildWarKill
        { get; set; }

        public uint GuildWarKilled
        { get; set; }

        public string NickName
        { get; set; }

        public uint RefObjID
        { get; set; }

        public GuildSiegeAuthority SiegeAuthority
        { get; set; }

        public GuildMemberOnlineStatus OnlineStatus
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public GuildMember()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketInnerEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.ID = packet.ReadValue<uint>();
            this.Name = packet.ReadValue<string>();

            this.MemberClass = packet.ReadEnum<GuildMemberClass>();

            this.Level = packet.ReadValue<byte>();
            this.GPDonation = packet.ReadValue<uint>();

            //Flags 
            this.Permission = packet.ReadEnum<GuildPermission>();
            this.Contribution = packet.ReadValue<uint>();
            this.GuildWarKill = packet.ReadValue<uint>();
            this.GuildWarKilled = packet.ReadValue<uint>();
            this.NickName = packet.ReadValue<string>();
            this.RefObjID = packet.ReadValue<uint>();

            this.SiegeAuthority = packet.ReadEnum<GuildSiegeAuthority>();
            this.OnlineStatus = packet.ReadEnum<GuildMemberOnlineStatus>();

            return true;
        }

        public bool AppendToPacket(XPacket packet)
        {
            packet.WriteValue<uint>(this.ID);
            packet.WriteValue<string>(this.Name);
            packet.WriteEnum<GuildMemberClass>(this.MemberClass);
            packet.WriteValue<byte>(this.Level);
            packet.WriteValue<uint>(this.GPDonation);
            packet.WriteEnum<GuildPermission>(this.Permission);
            packet.WriteValue<uint>(this.Contribution);
            packet.WriteValue<uint>(this.GuildWarKill);
            packet.WriteValue<uint>(this.GuildWarKilled);
            packet.WriteValue<string>(this.NickName);
            packet.WriteValue<uint>(this.RefObjID);
            packet.WriteEnum<GuildSiegeAuthority>(this.SiegeAuthority);
            packet.WriteEnum<GuildMemberOnlineStatus>(this.OnlineStatus);
            return true;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}

﻿using NLog;
using System;
using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// S->C SERVER_AGENT_GUILD_UPDATE_NOTICE = 0xB0F9
    /// TESTME
    /// </summary>
    public class GuildUpdateNoticeResponseEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties

        public PacketResultCode Code
        { get; private set; }

        public GuildErrorCode ErrorCode
        { get; private set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public GuildUpdateNoticeResponseEntity(PacketResultCode code)
        {
            this.Code = code;
            if (this.Code == PacketResultCode.Error)
                throw new ArgumentException("Error code must be supplied.");

        }

        public GuildUpdateNoticeResponseEntity(GuildErrorCode errorCode)
        {
            this.Code = PacketResultCode.Error;
            this.ErrorCode = errorCode;
        }


        public GuildUpdateNoticeResponseEntity()
        {
        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.Code = packet.ReadEnum<PacketResultCode>();
            if (this.Code == PacketResultCode.Error)
                this.ErrorCode = packet.ReadEnum<GuildErrorCode>();

            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.SERVER_AGENT_GUILD_UPDATE_NOTICE);

            packet.WriteEnum<PacketResultCode>(this.Code);

            if (this.Code == PacketResultCode.Error)
                packet.WriteEnum<GuildErrorCode>(this.ErrorCode);

            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}

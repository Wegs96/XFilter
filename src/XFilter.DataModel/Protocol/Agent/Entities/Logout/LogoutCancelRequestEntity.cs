﻿using NLog;
using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// C->S CLIENT_AGENT_LOGOUT_CANCEL_REQUEST = 0x7006
    /// TESTME
    /// </summary>
    public class LogoutCancelRequestEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public LogoutCancelRequestEntity()
        {

        }


        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.CLIENT_AGENT_LOGOUT_CANCEL_REQUEST);

            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}

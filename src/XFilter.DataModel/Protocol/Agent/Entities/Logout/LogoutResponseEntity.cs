﻿using NLog;
using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// S->C SERVER_AGENT_LOGOUT_RESPONSE = 0xB005
    /// TESTME
    /// </summary>
    public class LogoutResponseEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties

        public PacketResultCode Code
        { get; set; }

        /// <summary>
        /// Seconds.
        /// </summary>
        public byte Cooldown
        { get; set; }

        public LogoutMode Mode
        { get; set; }

        public LogoutErrorCode ErrorCode
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        /// <summary>
        /// Logout success msg ctor.
        /// </summary>
        /// <param name="cooldown">Cooldown.</param>
        /// <param name="mode">Logout mode.</param>
        public LogoutResponseEntity(byte cooldown, LogoutMode mode)
        {
            this.Code = PacketResultCode.Success;
            this.Cooldown = cooldown;
            this.Mode = mode;
        }

        /// <summary>
        /// Logout error msg ctor.
        /// </summary>
        /// <param name="errorCode"></param>
        public LogoutResponseEntity(LogoutErrorCode errorCode)
        {
            this.Code = PacketResultCode.Error;
            this.ErrorCode = errorCode;
        }

        public LogoutResponseEntity()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.Code = packet.ReadEnum<PacketResultCode>();

            if (this.Code == PacketResultCode.Success)
            {
                this.Cooldown = packet.ReadValue<byte>();
                this.Mode = packet.ReadEnum<LogoutMode>();
            }

            if (this.Code == PacketResultCode.Error)
                this.ErrorCode = packet.ReadEnum<LogoutErrorCode>();
            

            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.SERVER_AGENT_LOGOUT_RESPONSE);

            packet.WriteEnum<PacketResultCode>(this.Code);

            if(this.Code == PacketResultCode.Success)
            {
                packet.WriteValue<byte>(this.Cooldown);
                packet.WriteEnum<LogoutMode>(this.Mode);
            }

            if (this.Code == PacketResultCode.Error)
                packet.WriteEnum<LogoutErrorCode>(this.ErrorCode);

            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}

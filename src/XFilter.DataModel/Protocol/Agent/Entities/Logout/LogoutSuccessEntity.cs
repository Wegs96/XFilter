﻿using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// S->C SERVER_AGENT_LOGOUT_SUCCESS = 0x300A
    /// TESTME
    /// </summary>
    public class LogoutSuccessEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Constructors

        public LogoutSuccessEntity()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.SERVER_AGENT_LOGOUT_SUCCESS);
            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}

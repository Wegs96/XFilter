﻿using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// C->S CLIENT_AGENT_OBJECT_INTERACTION_START_REQUEST = 0x7045
    /// TESTME
    /// </summary>
    public class ObjectInteractionStartRequestEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Public properties

        public uint UniqueId
        { get; private set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public ObjectInteractionStartRequestEntity(uint uniqueId)
        {
            this.UniqueId = uniqueId;
        }

        public ObjectInteractionStartRequestEntity()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.UniqueId = packet.ReadValue<uint>();
            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.CLIENT_AGENT_OBJECT_INTERACTION_START_REQUEST);

            packet.WriteValue<uint>(this.UniqueId);

            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}

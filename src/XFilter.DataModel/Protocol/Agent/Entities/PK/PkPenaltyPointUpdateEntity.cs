﻿using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// S->C SERVER_AGENT_PK_UPDATE_PENALTY = 0x30CD
    /// TESTME
    /// </summary>
    public class PkPenaltyPointUpdateEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Public properties

        public uint PenaltyPoints
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public PkPenaltyPointUpdateEntity(uint points)
        {
            this.PenaltyPoints = points;
        }

        public PkPenaltyPointUpdateEntity()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.PenaltyPoints = packet.ReadValue<uint>();
            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.SERVER_AGENT_PK_UPDATE_PENALTY);
            packet.WriteValue<uint>(this.PenaltyPoints);
            return packet;
        }


        #endregion

        //--------------------------------------------------------------------------
    }
}

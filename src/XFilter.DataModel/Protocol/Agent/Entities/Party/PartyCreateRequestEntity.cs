﻿using NLog;
using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// C->S CLIENT_AGENT_PARTY_CREATE_REQUEST = 7060
    /// TESTME
    /// </summary>
    public class PartyCreateRequestEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties

        public uint TargetId
        { get; private set; }

        public PartySettingsFlag Settings
        { get; private set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public PartyCreateRequestEntity(PartySettingsFlag settings, uint targetId)
        {
            this.Settings = settings;
            this.TargetId = targetId;
        }

        public PartyCreateRequestEntity()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.Settings = packet.ReadEnum<PartySettingsFlag>();
            this.TargetId = packet.ReadValue<uint>();
   
            return true;

        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.CLIENT_AGENT_PARTY_CREATE_REQUEST);

            packet.WriteEnum<PartySettingsFlag>(this.Settings);
            packet.WriteValue<uint>(this.TargetId);


            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}

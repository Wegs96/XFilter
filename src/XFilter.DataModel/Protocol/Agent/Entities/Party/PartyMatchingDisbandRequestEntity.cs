﻿using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// C->S CLIENT_AGENT_PARTY_MATCHING_DISBAND_REQUEST = 0x706B
    /// TESTME
    /// </summary>
    public class PartyMatchingDisbandRequestEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Public properties

        public uint PartyId
        { get; private set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public PartyMatchingDisbandRequestEntity(uint partyId)
        {
            this.PartyId = partyId;
        }

        public PartyMatchingDisbandRequestEntity()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.PartyId = packet.ReadValue<uint>();
            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.CLIENT_AGENT_PARTY_MATCHING_DISBAND_REQUEST);

            packet.WriteValue<uint>(this.PartyId);

            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}

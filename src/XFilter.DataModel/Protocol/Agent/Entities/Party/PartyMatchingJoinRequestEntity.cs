﻿using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// C->S CLIENT_AGENT_PARTY_MATCHING_JOIN_REQUEST = 0x706D
    /// TESTME
    /// </summary>
    public class PartyMatchingJoinRequestEntity : IPacketEntity
    { 
        //--------------------------------------------------------------------------

        #region Public properties

        public uint PartyId
        { get; private set; }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.PartyId = packet.ReadValue<uint>();

            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.CLIENT_AGENT_PARTY_MATCHING_JOIN_REQUEST);

            packet.WriteValue<uint>(this.PartyId);

            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}

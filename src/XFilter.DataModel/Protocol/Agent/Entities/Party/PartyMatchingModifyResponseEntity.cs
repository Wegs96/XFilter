﻿using NLog;
using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// S->C SERVER_AGENT_PARTY_MATCHING_MODIFY_RESPONSE = 0xB06A
    /// TESTME
    /// </summary>
    public class PartyMatchingModifyResponseEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties

        public PacketResultCode Code
        { get; private set; }

        public PartyMatchingPartyModifyInfo PartyInfo
        { get; private set; }

        public PartyErrorCode ErrorCode
        { get; private set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public PartyMatchingModifyResponseEntity(PartyMatchingPartyModifyInfo info)
        {
            this.Code = PacketResultCode.Success;
            this.PartyInfo = info;
        }

        public PartyMatchingModifyResponseEntity(PartyErrorCode errorCode)
        {
            this.Code = PacketResultCode.Error;
            this.ErrorCode = errorCode;
        }

        public PartyMatchingModifyResponseEntity()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.Code = packet.ReadEnum<PacketResultCode>();
            this.PartyInfo = new PartyMatchingPartyModifyInfo();

            if (this.Code == PacketResultCode.Success)
                return this.PartyInfo.ReadFromPacket(packet);

            var errorCodeShort = packet.ReadValue<ushort>();

            this.ErrorCode = packet.ReadEnum<PartyErrorCode>();

            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.SERVER_AGENT_PARTY_MATCHING_MODIFY_RESPONSE);

            packet.WriteEnum<PacketResultCode>(this.Code);

            if (this.Code == PacketResultCode.Error)
            {
                packet.WriteEnum<PartyErrorCode>(this.ErrorCode);
            }
            else this.PartyInfo.AppendToPacket(packet);

            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}

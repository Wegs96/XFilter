﻿using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// C->S CLIENT_AGENT_STALL_TALK_REQUEST = 0x70B3
    /// TESTME
    /// </summary>
    public class StallTalkRequestEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Public properties

        public uint UniqueId
        { get; set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public StallTalkRequestEntity(uint uniqueId)
        {
            this.UniqueId = uniqueId;
        }

        public StallTalkRequestEntity()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            this.UniqueId = packet.ReadValue<uint>();

            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.CLIENT_AGENT_STALL_TALK_REQUEST);

            packet.WriteValue<uint>(this.UniqueId);

            return packet;
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}

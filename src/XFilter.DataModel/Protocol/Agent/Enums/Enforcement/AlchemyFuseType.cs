﻿namespace XFilter.DataModel.Protocol.Agent
{
    //--------------------------------------------------------------------------

    public enum AlchemyFuseType : byte
    {
        Disjoin = 1,
        Manufacture = 2,
        Elixir = 3,
        MagicStone = 4,
        AttrStone = 5,
        AdvancedElixir = 8,
        Socket = 9
    }

    //--------------------------------------------------------------------------
}

﻿namespace XFilter.DataModel.Protocol.Agent
{
    //--------------------------------------------------------------------------

    /// <summary>
    /// See DEC_FriendListErrorCodeRelated_sub_5D7620@<ax>(_DWORD *a1@<ecx>, _DWORD *a2@<ebx>)
    /// </summary>
    public enum FriendAddResultCode : ushort
    {
        Success = 0x2,

        UIIT_MSG_FRIENDERR_FULLIST = 0x6408,
        UIIT_MSG_FRIENDERR_ALREADYFRIEND = 0x6409,
        UIIT_MSG_FRIENDERR_SYSCANCEL = 0x640A,

        //Contains 1 byte + string with charname who refused
        UIIT_MSG_FRIENDERR_REFUSED_OR_ACCEPTED = 0x640B,

        //User offline
        UIIT_MSG_FRIENDERR_UNKNOWNUSER = 0x640D,
        UIIT_MSG_FRIENDERR_SYSPROGRESS = 0x640E,
        UIIT_MSG_FRIENDERR_TARGETLISTFULL = 0x640F,
    }

    //--------------------------------------------------------------------------
}

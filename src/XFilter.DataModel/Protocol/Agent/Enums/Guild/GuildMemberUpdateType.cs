﻿namespace XFilter.DataModel.Protocol.Agent
{
    //--------------------------------------------------------------------------

    public enum GuildMemberUpdateType : byte
    {
        Level = 1,
        State = 2,
        GuildPoints = 8,
        SiegeAuthority = 64,
    }

    //--------------------------------------------------------------------------
}

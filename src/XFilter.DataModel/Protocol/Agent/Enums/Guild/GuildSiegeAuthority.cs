﻿using System;

namespace XFilter.DataModel.Protocol.Agent
{
    //--------------------------------------------------------------------------

    [Flags]
    public enum GuildSiegeAuthority : byte
    {
        None = 0,
        Commander = 1,
        DeputyCommander =2,
        FortressWarAdministrator = 4,
        ProductionAdministrator = 8,
        TrainingAdministrator = 16,
        MilitaryEngineer = 32,
    }

    //--------------------------------------------------------------------------
}

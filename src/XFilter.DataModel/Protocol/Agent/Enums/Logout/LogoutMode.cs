﻿namespace XFilter.DataModel.Protocol.Agent
{
    //--------------------------------------------------------------------------

    public enum LogoutMode : byte
    {
        /// <summary>
        /// CPSQuit
        /// </summary>
        Exit = 1,
        
        /// <summary>
        /// CPSRestart
        /// </summary>
        Restart = 2,
    }

    //--------------------------------------------------------------------------
}

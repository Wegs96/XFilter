﻿namespace XFilter.DataModel.Protocol.Agent
{
    //--------------------------------------------------------------------------

    public enum PartyType : byte
    {
        Hunting = 0,
        Quest = 1,
        Trade = 2,
        Thief = 3,
    }

    //--------------------------------------------------------------------------
}

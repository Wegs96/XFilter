﻿namespace XFilter.DataModel.Protocol.Agent
{
    //--------------------------------------------------------------------------

    public enum StallNetworkResult : ushort
    { 
        /// <summary>
        /// Stall net not implemented @ VSRO 188.
        /// </summary>
        None = 0,
        Success = 1,
    }

    //--------------------------------------------------------------------------
}

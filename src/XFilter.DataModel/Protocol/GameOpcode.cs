﻿namespace XFilter.DataModel.Protocol
{
    /// <summary>
    /// The server / client opcode map. 
    /// </summary>
    public enum GameOpcode : ushort
    {
        //--------------------------------------------------------------------------

        #region Global

        /// <summary>
        /// Module name / type identification (global).
        /// </summary>
        GLOBAL_MODULE_IDENTIFY = 0x2001,

        /// <summary>
        /// The global handshake packet.
        /// </summary>
        GLOBAL_HANDSHAKE = 0x5000,

        #endregion

        //--------------------------------------------------------------------------

        #region Client -> Server

        /// <summary>
        /// The client ping request.
        /// </summary>
        CLIENT_PING_REQUEST = 0x2002,

        /// <summary>
        /// The client handshake response packet.
        /// </summary>
        CLIENT_HANDSHAKE_RESPONSE = 0x9000,

        /// <summary>
        /// Gateway client -> server news info request msg.
        /// </summary>
        CLIENT_GATEWAY_LAUNCHER_NEWS_REQUEST = 0x6104,

        /// <summary>
        /// The clients captcha answer packet.
        /// </summary>
        CLIENT_GATEWAY_CAPTCHA_ANSWER = 0x6323,

        /// <summary>
        /// Gateway client -> server shard list update request.
        /// </summary>
        CLIENT_GATEWAY_SHARD_LIST_PING_REQUEST = 0x6106,

        /// <summary>
        /// The client -> server login packet.
        /// </summary>
        CLIENT_GATEWAY_LOGIN_REQUEST = 0x6102,

        /// <summary>
        /// Gateway client -> server server list request msg.
        /// </summary>
        CLIENT_GATEWAY_SERVERLIST_REQUEST = 0x6101,

        /// <summary>
        /// Gateway client -> server patch info request msg. Encrypted.
        /// </summary>
        CLIENT_GATEWAY_PATCH_REQUEST = 0x6100,

        /// <summary>
        /// The client -> server agent auth request.
        /// </summary>
        CLIENT_AGENT_AUTH_REQUEST = 0x6103,

        /// <summary>
        /// Agent client -> server chat request msg.
        /// </summary>
        CLIENT_AGENT_CHAT_REQUEST = 0x7025,

        /// <summary>
        /// Agent client -> server packet when u click on char enter game button.
        /// </summary>
        CLIENT_AGENT_CHARACTER_SELECTION_JOIN_REQUEST = 0x7001,

        /// <summary>
        /// Agent client -> server action request (creation, deletion, name check ,restore).
        /// </summary>
        CLIENT_AGENT_CHARACTER_SELECTION_ACTION_REQUEST = 0x7007,

        /// <summary>
        /// Agent client -> server requesting for name change msg.
        /// </summary>
        CLIENT_AGENT_CHARACTER_SELECTION_RENAME_REQUEST = 0x7450,

        /// <summary>
        /// Agent client -> server logout request.
        /// </summary>
        CLIENT_AGENT_LOGOUT_REQUEST = 0x7005,

        /// <summary>
        /// Agent client -> serve logout cancel request msg.
        /// </summary>
        CLIENT_AGENT_LOGOUT_CANCEL_REQUEST = 0x7006,

        /// <summary>
        /// Agent client -> server guide request msg.
        /// </summary>
        CLIENT_AGENT_GUIDE_REQUEST = 0x70EA,

        /// <summary>
        /// Agent client -> server FRPVP request msg.
        /// </summary>
        CLIENT_AGENT_FRPVP_UPDATE_REQUEST = 0x7516,

        /// <summary>
        /// Agent client -> server stall create request msg.
        /// </summary>
        CLIENT_AGENT_STALL_CREATE_REQUEST = 0x70B1,

        /// <summary>
        /// Agent client -> server stall content update request msg.
        /// </summary>
        CLIENT_AGENT_STALL_UPDATE_REQUEST = 0x70BA,

        /// <summary>
        /// Agent client -> server stall destruction request msg.
        /// </summary>
        CLIENT_AGENT_STALL_DESTROY_REQUEST = 0x70B2,


        /// <summary>
        /// Agent client -> server guild master notice update request.
        /// </summary>
        CLIENT_AGENT_GUILD_UPDATE_NOTICE = 0x70F9,

        /// <summary>
        /// Agent client -> server stall leave request msg.
        /// </summary>
        CLIENT_AGENT_STALL_LEAVE_REQUEST = 0x70B5,

        /// <summary>
        /// Agent client -> server stall talk request msg.
        /// Once some player enters others stall.
        /// </summary>
        CLIENT_AGENT_STALL_TALK_REQUEST = 0x70B3,

        /// <summary>
        /// Agent client -> server stall buy request msg.
        /// </summary>
        CLIENT_AGENT_STALL_BUY_REQUEST = 0x70B4,

        /// <summary>
        /// Agent client -> server movement request msg.
        /// </summary>
        CLIENT_AGENT_MOVEMENT_REQUEST = 0x7021,

        /// <summary>
        /// Agent client -> server alchemy item reinforce request smg.
        /// </summary>
        CLIENT_AGENT_ENFORCEMENT_REINFORCE_REQUEST = 0x7150,

        /// <summary>
        /// Agent client -> server magic option grant request (avatar, mby smth else).
        /// </summary>
        CLIENT_AGENT_ENFORCEMENT_MAGIC_OPTION_GRANT_REQUEST = 0x34A9,

        /// <summary>
        /// Agent client -> server object action request.
        /// </summary>
        CLIENT_AGENT_OBJECT_ACTION_REQUEST = 0x7074,

        /// <summary>
        /// Agent client -> server object interaction start request.
        /// </summary>
        CLIENT_AGENT_OBJECT_INTERACTION_START_REQUEST = 0x7045,

        /// <summary>
        /// Agent client -> server designate as return / recall point request msg.
        /// </summary>
        CLIENT_AGENT_MOVEMENT_DESIGNATE_AS_RECALL_POINT_REQUEST = 0x7059,

        /// <summary>
        /// Agent client -> server friend add request msg.
        /// </summary>
        CLIENT_AGENT_FRIEND_ADD_REQUEST = 0x7302,

        /// <summary>
        /// Agent client -> server requests for interaction stop.
        /// </summary>
        CLIENT_AGENT_OBJECT_INTERACTION_END_REQUEST = 0x704B,

        /// <summary>
        /// Agent client -> server teleport request msg.
        /// </summary>
        CLIENT_AGENT_MOVEMENT_TELEPORT_REQUEST = 0x705A,

        /// <summary>
        /// Agent client -> server party matching request msg.
        /// </summary>
        CLIENT_AGENT_PARTY_MATCHING_REQUEST = 0x706C,

        /// <summary>
        /// Agent client -> server player trying to join party from 
        /// party matching request msg.
        /// </summary>
        CLIENT_AGENT_PARTY_MATCHING_JOIN_REQUEST = 0x706D,

        /// <summary>
        /// Agent client -> server xtrap challenge response.
        /// </summary>
        CLIENT_AGENT_XTRAP_CHALLENGE_RESPONSE = 0x2113,

        /// <summary>
        /// Agent client -> server party CREATION msg.
        /// </summary>
        CLIENT_AGENT_PARTY_CREATE_REQUEST = 0x7060,

        /// <summary>
        /// Agent client -> server party INVITATION msg.
        /// </summary>
        CLIENT_AGENT_PARTY_INVITE_REQUEST = 0x7062,

        /// <summary>
        /// Agent client -> server party leave request msg.
        /// </summary>
        CLIENT_AGENT_PARTY_LEAVE_REQUEST = 0x7061,

        /// <summary>
        /// Agent client -> server party kick request msg.
        /// </summary>
        CLIENT_AGENT_PARTY_KICK_REQUEST = 0x7063,

        /// <summary>
        /// Agent client -> server master is removing party from matching msg.
        /// </summary>
        CLIENT_AGENT_PARTY_MATCHING_DISBAND_REQUEST = 0x706B,

        /// <summary>
        /// Agent client -> server master tries to modify party info.
        /// </summary>
        CLIENT_AGENT_PARTY_MATCHING_MODIFY_REQUEST = 0x706A,

        #endregion

        //--------------------------------------------------------------------------

        #region Server -> Client

        /// <summary>
        /// The server -> client captcha challenge.
        /// </summary>
        SERVER_GATEWAY_CAPTCHA_CHALLENGE = 0x2322,

        /// <summary>
        /// Download server info server -> client.
        /// </summary>
        SERVER_GATEWAY_DOWNLOAD_RESPONSE = 0xA100,

        /// <summary>
        /// The global operation & shard list server -> client packet.
        /// </summary>
        SERVER_GATEWAY_SHARD_LIST_RESPONSE = 0xA101,

        /// <summary>
        /// Gateway server -> client shard list ping response msg.
        /// </summary>
        SERVER_GATEWAY_SHARD_LIST_PING_RESPONSE = 0xA106,


        /// <summary>
        /// The server -> client agent auth response.
        /// </summary>
        SERVER_AGENT_AUTH_RESPONSE = 0xA103,

        /// <summary>
        /// Agent server (or error on login) server -> client msg.
        /// </summary>
        SERVER_GATEWAY_LOGIN_RESPONSE = 0xA102,

        /// <summary>
        /// Agent server -> client chat update msg.
        /// </summary>
        SERVER_AGENT_CHAT_UPDATE = 0x3026,

        /// <summary>
        /// Agent server -> client char rename response msg.
        /// </summary>
        SERVER_AGENT_CHARACTER_SELECTION_RENAME_RESPONSE = 0xB450,

        /// <summary>
        /// Agent server -> client char selection join response.
        /// </summary>
        SERVER_AGENT_CHARACTER_SELECTION_JOIN_RESPONSE = 0xB001,

        /// <summary>
        /// Agent server -> client chat response code.
        /// </summary>
        SERVER_AGENT_CHAT_RESPONSE = 0xB025,

        /// <summary>
        /// Agent server -> client logout success msg.
        /// </summary>
        SERVER_AGENT_LOGOUT_SUCCESS = 0x300A,

        /// <summary>
        /// Agent server -> client logout response msg.
        /// </summary>
        SERVER_AGENT_LOGOUT_RESPONSE = 0xB005,

        /// <summary>
        /// Agent server -> client logout cancel result msg.
        /// </summary>
        SERVER_AGENT_LOGOUT_CANCEL_RESPONSE = 0xB006,

        /// <summary>
        /// Agent server -> client guide response msg.
        /// </summary>
        SERVER_AGENT_GUIDE_RESPONSE = 0xB0EA,

        /// <summary>
        /// Agent server -> client weather update msg.
        /// </summary>
        SERVER_AGENT_ENVIRONMENT_WEATHER_UPDATE = 0x3809,

        /// <summary>
        /// Agent server -> client celestial update msg.
        /// </summary>
        SERVER_AGENT_ENVIRONMENT_CELESTIAL_UPDATE = 0x3027,

        /// <summary>
        /// Agent server -> client celestial position msg.
        /// </summary>
        SERVER_AGENT_ENVIRONMENT_CELESTIAL_POSITION = 0x3020,
        
        /// <summary>
        /// Agent server -> client PK penalty point msg.
        /// </summary>
        SERVER_AGENT_PK_UPDATE_PENALTY = 0x30CD,

        /// <summary>
        ///  Agent server -> client PK count update msg.
        /// </summary>
        SERVER_AGENT_PK_UPDATE_DAILY = 0x30CE,

        /// <summary>
        /// Agent server -> client PK level update msg.
        /// </summary>
        SERVER_AGENT_PK_UPDATE_LEVEL = 0x30D3,

        /// <summary>
        /// Gateway server -> client news info response msg.
        /// </summary>
        SERVER_GATEWAY_LAUNCHER_NEWS_RESPONSE = 0xA104,

        /// <summary>
        /// Gateway server -> client captcha input result msg.
        /// </summary>
        SERVER_GATEWAY_CAPTCHA_RESULT = 0xA323,

        /// <summary>
        /// Agent server -> client char action response msg.
        /// </summary>
        SERVER_AGENT_CHARACTER_SELECTION_RESPONSE = 0xB007,

        /// <summary>
        /// Agent server -> client FRPVP response.
        /// </summary>
        SERVER_AGENT_FRPVP_UPDATE_RESPONSE = 0xB516,

        /// <summary>
        /// Agent server -> client stall creation response.
        /// </summary>
        SERVER_AGENT_STALL_CREATE_RESPONSE = 0xB0B1,

        /// <summary>
        /// Agent server -> client stall update response.
        /// </summary>
        SERVER_AGENT_STALL_UPDATE_RESPONSE = 0xB0BA,

        /// <summary>
        /// Agent server -> client stall destruction response.
        /// </summary>
        SERVER_AGENT_STALL_DESTROY_RESPONSE = 0xB0B2,

        /// <summary>
        /// Agent server -> client stall leaving response.
        /// </summary>
        SERVER_AGENT_STALL_LEAVE_RESPONSE = 0xB0B5,

        /// <summary>
        /// Agent server -> client stall talk (stall content) response.
        /// </summary>
        SERVER_AGENT_STALL_TALK_RESPONSE = 0xB0B3,

        /// <summary>
        /// Agent server -> client stall buy response.
        /// </summary>
        SERVER_AGENT_STALL_BUY_RESPONSE = 0xB0B4,

        /// <summary>
        /// Agent server -> client union info response.
        /// </summary>
        SERVER_AGENT_UNION_INFO_RESPONSE = 0x3102,

        /// <summary>
        /// Agent server -> client guild update response.
        /// </summary>
        SERVER_AGENT_GUILD_UPDATE_RESPONSE = 0x38F5,

        /// <summary>
        /// Agent server -> client guild master notice update response.
        /// </summary>
        SERVER_AGENT_GUILD_UPDATE_NOTICE = 0xB0F9,

        /// <summary>
        /// Agent server -> client movement response.
        /// </summary>
        SERVER_AGENT_MOVEMENT_RESPONSE = 0xB021,

        /// <summary>
        /// Agent server -> client alchemy response msg.
        /// </summary>
        SERVER_AGENT_ALCHEMY_REINFORCE_RESPONSE = 0xB150,

        /// <summary>
        /// Agent server -> client designate as return / recall point response msg.
        /// </summary>
        SERVER_AGENT_MOVEMENT_DESIGNATE_AS_RECALL_POINT_RESPONSE = 0xB059,

        /// <summary>
        /// Agent server -> client object interaction end msg.
        /// </summary>
        SERVER_AGENT_OBJECT_INTERACTION_END_RESPONSE = 0xB04B,

        /// <summary>
        /// Agent server -> client friend add result msg.
        /// </summary>
        SERVER_AGENT_FRIEND_ADD_RESULT_RESPONSE = 0xB302,

        /// <summary>
        /// Agent server -> server friend add request msg.
        /// </summary>
        SERVER_AGENT_FRIEND_ADD_REQUEST_RESPONSE = 0x7302,

        /// <summary>
        /// Agent server -> client challenge msg.
        /// </summary>
        SERVER_AGENT_XTRAP_CHALLENGE = 0x2113,

        /// <summary>
        /// Agent server -> client party disband from matching result msg.
        /// </summary>
        SERVER_AGENT_PARTY_MATCHING_DISBAND_RESPONSE = 0xB06B,

        /// <summary>
        /// Agent server -> client party matching modify response msg.
        /// </summary>
        SERVER_AGENT_PARTY_MATCHING_MODIFY_RESPONSE = 0xB06A,

        /// <summary>
        /// Agent server -> client party matching list (page) response msg.
        /// </summary>
        SERVER_AGENT_PARTY_MATCHING_PAGE_RESPONSE = 0xB06C,

        
        /// <summary>
        /// Agent server -> client exchange invitation msg.
        /// </summary>
        SERVER_AGENT_EXCHANGE_START = 0x7081,

        //--------------------------------------------------------------------------

        #endregion

        //--------------------------------------------------------------------------

        //Has to be processed when SERVER_AGENT_GUILD_INFO_END fired.
        #region SPLIT - Guild info

        /// <summary>
        /// Agent server -> client guild info start.
        /// </summary>
        SERVER_AGENT_GUILD_INFO_BEGIN = 0x34B3,

        /// <summary>
        /// Agent server -> guild info message.
        /// </summary>
        SERVER_AGENT_GUILD_INFO_DATA = 0x3101,

        /// <summary>
        /// Agent server -> client guild info end. PROCESS NOW !
        /// </summary>
        SERVER_AGENT_GUILD_INFO_END = 0x34B4,



        #endregion

        /*
        #region SPLIT - Char info

        /// <summary>
        /// Agent server -> client char data begin.
        /// </summary>
        SERVER_AGENT_CHARACTER_INFO_BEGIN = 0x34A5,

        /// <summary>
        /// Agent server -> client char info message.
        /// </summary>
        SERVER_AGENT_CHARACTER_INFO_DATA = 0x3013,

        /// <summary>
        /// Agent server -> client char info end.
        /// </summary>
        SERVER_AGENT_CHARACTER_INFO_END = 0x34A6,

        #endregion
        */

        //--------------------------------------------------------------------------
    }
}

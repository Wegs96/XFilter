﻿using System.Collections.Generic;
using XFilter.Shared.Network;


namespace XFilter.DataModel.Protocol.Gateway
{
    /// <summary>
    /// S->C SERVER_GATEWAY_LAUNCHER_NEWS_RESPONSE = 0xA104, Massive
    /// TESTME
    /// </summary>
    public class LauncherNewsEntity : IPacketEntity
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private List<LauncherNewsItem> _articles;

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public LauncherNewsEntity()
        {
        }

        #endregion

        //--------------------------------------------------------------------------

        #region IPacketEntity

        public bool ReadFromPacket(XPacket packet)
        {
            _articles = new List<LauncherNewsItem>();

            var count = packet.ReadValue<byte>();

            for (int i = 0; i < count; i++)
            {
                var item = new LauncherNewsItem();
                item.ReadFromPacket(packet);
                _articles.Add(item);
            }

            return true;
        }

        public XPacket WriteToPacket()
        {
            var packet = new XPacket((ushort)GameOpcode.SERVER_GATEWAY_LAUNCHER_NEWS_RESPONSE, false, true);

            var articleCountByte = (byte)_articles.Count;

            packet.WriteValue<byte>(articleCountByte);
            foreach(var article in _articles)
                article.AppendToPacket(packet);

            return packet;
        }

        public void AddArticle(LauncherNewsItem item) => _articles.Add(item);
        public void ClearArticles() => _articles.Clear();


        #endregion

        //--------------------------------------------------------------------------
    }
}

﻿namespace XFilter.DataModel.Protocol.Gateway
{
    //--------------------------------------------------------------------------

    public enum LoginResultCode : byte
    {
        /// <summary>
        /// Login successful - send agent info.
        /// </summary>
        Success = 0x01,

        /// <summary>
        /// Error happened while trying to login(max login attempts or ban).
        /// </summary>
        Error = 0x02,

        /// <summary>
        /// Not supported by every client.
        /// </summary>
        Custom = 0x03,
    }

    //--------------------------------------------------------------------------
}

﻿namespace XFilter.DataModel.Protocol
{
    public enum PacketResultCode : byte
    {
        Success = 0x01,
        Error = 0x02,
    }
}

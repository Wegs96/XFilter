﻿using System.Collections.Generic;
using XFilter.Shared.Network;

namespace XFilter.DataModel.Protocol
{
    /// <summary>
    /// Split packet interface.
    /// </summary>
    interface ISplitPacket
    {
        List<XPacket> ToPackets();
    }
}

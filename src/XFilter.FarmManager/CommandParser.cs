﻿using NLog;
using System;
using System.Text;
using System.Threading;

namespace XFilter.FarmManager
{
    internal sealed class CommandParser
    {
        //--------------------------------------------------------------------------

        #region Private properties

        private CancellationTokenSource _cts;
        private CancellationToken _ct;

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public CommandParser()
        {
            _cts = new CancellationTokenSource();
            _ct = _cts.Token;
        }

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        /// <summary>
        /// Starts reading commands from the user.
        /// </summary>
        public void Initialize() => CommandProcessWorker();

        /// <summary>
        /// Aborts the command parser task.
        /// </summary>
        public void Deinitialize() => 
            _cts.Cancel();

        /// <summary>
        /// Remove spaces.
        /// </summary>
        /// <param name="args">Args.</param>
        private void TrimArgs(ref string[] args)
        {
            for(int i = 0; i < args.Length; i++)
                args[i] = args[i].Trim();
        }

        /// <summary>
        /// Prints the list of available commands.
        /// </summary>
        private void PrintHelpCommands()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(gs.CommentLine);
            builder.Append("List of available commands:" + Environment.NewLine);
            builder.Append("/help - list available commands." + Environment.NewLine);
            builder.Append("/clear - clear the console." + Environment.NewLine);
            builder.Append("/netstate - print the state of network engine." + Environment.NewLine);
            Logger.Info(builder.ToString());
        }

        /// <summary>
        /// Clears the console window.
        /// </summary>
        private void ClearConsole()
        {
            Console.Clear();
            Logger.Info("Console cleared.");
        }

        /// <summary>
        /// Kindly asks if you wanna close the app.
        /// </summary>
        private void ExitApp()
        {
            Logger.Info(gs.CommentLine);
            Logger.Info("Are you sure ? y/n" + Environment.NewLine);
            if(Console.ReadLine().ToLower() == "y")
            {
                gs.Service.Farm.Deinitialize();
                Console.ReadLine();

                Environment.Exit(0);
            }
        }

        private void PrintNetState()
        {
            Logger.Info(gs.Service.Farm.DumpServiceState());
        }

        private void RefreshSlaves()
        {
            Logger.Info("Reconfiguring slave servers.");
            //gs.Service.Farm.ContextManager.
        }


        /// <summary>
        /// Main command dispatcher worker.
        /// </summary>
        private void CommandProcessWorker()
        {
            PrintHelpCommands();


            while(true)
            {
                if (_ct.IsCancellationRequested)
                    break;

                string[] cmds = Console.ReadLine().Split(new char[] { ' ' });
                TrimArgs(ref cmds);

                switch (cmds[0])
                {
                    case "/help":
                        {
                            PrintHelpCommands();
                        }
                        break;
                    case "/clear":
                        {
                            ClearConsole();
                        }
                        break;
                    case "/netstate":
                        {
                            PrintNetState();
                        }
                        break;
                    case "/refresh":
                        {
                            RefreshSlaves();
                        }
                        break;
                    case "/exit":
                        {
                            ExitApp();
                        }
                        break;
                }
            }
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}

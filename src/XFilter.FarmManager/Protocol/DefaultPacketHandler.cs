﻿namespace XFilter.FarmManager.Protocol.Farm
{
    using NLog;
    using XFilter.FarmManager.Network;
    using XFilter.FarmManager.Services.Farm;
    using XFilter.Shared.Network;


    class DefaultPacketHandler : IFarmPacketHandler
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public void Execute(FarmUserContext context, XPacket packet)
        {
            /* DEBUGMSG
            Logger.Error("[SMC] Client -> Server 0x{0:X}, Massive {1} Encrypted {2} {3}{4}",
                packet.Opcode, packet.Massive, packet.Encrypted,
                Environment.NewLine,
                DebugHelper.HexDump(packet.GetBytes()));
                */
        }
    }
}

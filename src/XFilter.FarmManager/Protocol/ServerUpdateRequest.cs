﻿namespace XFilter.FarmManager.Protocol.Farm
{
    using NLog;
    using System.Linq;
    using XFilter.DataModel.DTO.Entities;
    using XFilter.DataModel.Models;
    using XFilter.DataModel.Network.Ipc;
    using XFilter.FarmManager.Services.Farm;
    using XFilter.Shared.Helpers;
    using XFilter.Shared.Network;

    class ServerUpdateRequest
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Logic - start single instance (by ref server id)

        private static void StartSingle(FarmUserContext slave, int refSrvId)
        {
            XPacket packet = new XPacket((ushort)IpcOpcode.SlaveServerUpdate);
            packet.WriteValue<int>((int)IpcSlaveUpdateOp.StartOrRestart);

            if (!slave.State.IsHandshaked)
            {
                Logger.Error($"Attacker detected. Trying to exploit 0x{0:X} IPC opcode. IP: {1}. Dropping context.",
                    packet.Opcode, slave.State.IpStr);
                gs.Service.Farm.ContextManager.Stop(slave);
                return;
            }

            var refServer = gs.Service.Farm.DatabaseManager.C_Servers.FirstOrDefault(
                serv => serv.ID == refSrvId);

            if(refServer == null)
            {
                Logger.Error("Trying to restart/start unknown ref server id.");
                return;
            }

            //Only 1 server has to be restarted :).
            packet.WriteValue<int>(1);

            if (refServer.ServerType == ServerType.Gateway)
            {
                var configEntity = GatewayConfigEntity.FromModel(gs.Service.Farm.DatabaseManager, refSrvId);
                //Send our json object to the slave server :)
                packet.WriteValue<int>(ServerType.Gateway);
                packet.WriteValue<string>(PersistenceHelper.ToJson<GatewayConfigEntity>(configEntity));
            }

            if(refServer.ServerType == ServerType.Download)
            {
                var configEntity = DownloadConfigEntity.FromModel(gs.Service.Farm.DatabaseManager, refSrvId);
                //Send our json object to the slave server
                packet.WriteValue<int>(ServerType.Download);
                packet.WriteValue<string>(PersistenceHelper.ToJson<DownloadConfigEntity>(configEntity));
            }

            if(refServer.ServerType == ServerType.Agent)
            {
                var configEntity = AgentConfigEntity.FromModel(gs.Service.Farm.DatabaseManager, refSrvId);
                //Send our json object to the slave server :)
                packet.WriteValue<int>(ServerType.Agent);
                packet.WriteValue<string>(PersistenceHelper.ToJson<AgentConfigEntity>(configEntity));
            }

            slave.Send(packet);

            Logger.Info("Single server restart request sent.");
        }

        #endregion

        //--------------------------------------------------------------------------

        #region Logic - start all slave related services by ref server id

        private static void StartAll(FarmUserContext slave)
        {
            XPacket packet = new XPacket((ushort)IpcOpcode.SlaveServerUpdate);
            packet.WriteValue<int>((int)IpcSlaveUpdateOp.StartOrRestart);

            if (!slave.State.IsHandshaked)
            {
                Logger.Error($"Attacker detected. Trying to exploit 0x{0:X} IPC opcode. IP: {1}. Dropping context.",
                    packet.Opcode, slave.State.IpStr);
                gs.Service.Farm.ContextManager.Stop(slave);
                return;
            }

            Logger.Info($"Slave ID {slave.State.Id} information request.");

            //Send as JSON
            //Fetch the server entities from the DB
            var refServers = gs.Service.Farm.DatabaseManager.C_SlaveServerMappings.Where(srv => srv.SlaveServerID == slave.State.Id);
            var refServersList = refServers.ToList();
            packet.WriteValue<int>(refServersList.Count);

            refServersList.ForEach(serverMapping =>
            {
                var refServer = gs.Service.Farm.DatabaseManager.C_Servers.First(srv => srv.ID == serverMapping.RefServerID);
                ServerType type = (ServerType)refServer.ServerType;

                if (type == ServerType.Gateway)
                {
                    var configEntity = GatewayConfigEntity.FromModel(gs.Service.Farm.DatabaseManager, (int)serverMapping.RefServerID);
                    //Send our json object to the slave server :)
                    packet.WriteValue<int>(type);
                    packet.WriteValue<string>(PersistenceHelper.ToJson<GatewayConfigEntity>(configEntity));
                    Logger.Warn($"Sent gateway svc startup/restart command for slave id {refServer.ID}");
                }

                if (type == ServerType.Download)
                {
                    var configEntity = DownloadConfigEntity.FromModel(gs.Service.Farm.DatabaseManager, (int)serverMapping.RefServerID);
                    //Send our json object to the slave server :)
                    packet.WriteValue<int>(type);
                    packet.WriteValue<string>(PersistenceHelper.ToJson<DownloadConfigEntity>(configEntity));

                    Logger.Warn($"Sent download svc startup/restart command for slave id {refServer.ID}");
                }

                if (type == ServerType.Agent)
                {
                    var configEntity = AgentConfigEntity.FromModel(gs.Service.Farm.DatabaseManager, (int)serverMapping.RefServerID);
                    //Send our json object to the slave server.
                    packet.WriteValue<int>(type);
                    packet.WriteValue<string>(PersistenceHelper.ToJson<AgentConfigEntity>(configEntity));

                    Logger.Warn($"Sent agent svc startup/restart command for slave id {refServer.ID}");
                }
            });

            slave.Send(packet);
        }

        #endregion

        //--------------------------------------------------------------------------

        #region Logic - Dispatcher

        public static void StartOrRestart(int slaveId, int refSrvId = -1)
        {
            var slave = gs.Service.Farm.ContextManager.GetContextByServerId(slaveId);

            if (slave == null)
            {
                Logger.Error($"Slave with id {slaveId} is not connected.");
                return;
            }

            //All associated SR services must be restarted.
            if (refSrvId == -1)
            {
                StartAll(slave); 
            }
            else
            {
                StartSingle(slave, refSrvId);
            }
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}

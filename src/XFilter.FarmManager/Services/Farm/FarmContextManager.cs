﻿namespace XFilter.FarmManager.Services.Farm
{
    using NLog;
    using System;
    using System.Collections.Generic;
    using System.Net.Sockets;
    using System.Threading.Tasks;
    using XFilter.Shared.Network;


    /// <summary>
    /// NOTE: Improveme
    /// </summary>
    public sealed class FarmContextManager
    {
        //--------------------------------------------------------------------------

        #region Private properties, constants & statics

        private List<FarmUserContext> _contexts;

        private readonly object _lock = new object();
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        /// <summary>
        /// The ctor.
        /// </summary>
        public FarmContextManager()
        {
           
        }

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties

        /// <summary>
        /// The count of currently connected client contexts.
        /// </summary>
        public int Count => _contexts.Count;

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        /// <summary>
        /// Initializes the context manager.
        /// </summary>
        /// <returns></returns>
        public bool Initialize()
        {
            var netCfg = gs.Service.Farm.Settings.NetEngineConfig;

            _contexts = new List<FarmUserContext>((int)netCfg.MaxConnections);
            return true;
        }

  
        /// <summary>
        /// Required for object pool.
        /// </summary>
        /// <returns>ClientContext.</returns>
        private FarmUserContext GenerateDefaultUserContext() => new FarmUserContext();

        /// <summary>
        /// Enqueues user context startup (as task).
        /// </summary>
        /// <param name="client">The client socket.</param>
        public void EnqueueStartup(Socket client) => Task.Factory.StartNew(() => Start(client));

        private void Start(object sockObj)
        {
            var client = sockObj as Socket;

            var context = new FarmUserContext();
            context.Start(client);

            lock (_lock)
                _contexts.Add(context);
        }

        /// <summary>
        /// Stops the given client context.
        /// </summary>
        /// <param name="context">The client context.</param>
        public void Stop(FarmUserContext context)
        {
            context.Stop();

            lock(_lock)
                _contexts.Remove(context);
        }

        /// <summary>
        /// Terminates all currently connected user contexts.
        /// </summary>
        public void StopAll()
        {
            lock(_lock)
            {
                foreach (var context in _contexts)
                    context.Stop();
                _contexts.Clear();
            }
        }

        /// <summary>
        /// Broadcasts packet to all connected contexts.
        /// </summary>
        /// <param name="packet">The packet for broadcast.</param>
        public void BroadcastPacket(XPacket packet)
        {
            lock(_lock)
            {
                foreach (var context in _contexts)
                    context.Send(packet, false);
            }
        }

        /// <summary>
        /// Dumps the context manager state.
        /// </summary>
        /// <returns>The context manager state string.</returns>
        public string DumpState()
        {
            return $"Context count: {this.Count}" + Environment.NewLine;
        }

        /// <summary>
        /// Gets the user context by server id.
        /// </summary>
        /// <param name="refServerId"></param>
        /// <returns></returns>
        public FarmUserContext GetContextByServerId(int slaveId)
        {
            lock(_lock)
            {
                foreach(var context in _contexts)
                {
                    if (context.State.Id == slaveId)
                        return context;
                }
            }
            return null;
        }
        #endregion

        //--------------------------------------------------------------------------
    }
}

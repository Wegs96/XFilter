﻿namespace XFilter.FarmManager.Services
{
    using System;
    using System.Threading;
    using XFilter.FarmManager.Services.Farm;


    public sealed class ServiceManager
    {
        public FarmService Farm
        { get; private set; }

        void InfoReporterTaskWorker()
        {
            while (true)
            {

                System.Threading.Thread.Sleep(100);
            }
        }

        public bool Initialize()
        {
            this.Farm = new FarmService();
            bool farmStarted = this.Farm.Initialize();

            if(farmStarted)
                new Thread(InfoReporterTaskWorker).Start();

            return farmStarted;
        }
    }
}

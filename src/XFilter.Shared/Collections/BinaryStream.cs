﻿using System;
using System.Collections.Generic;
using System.IO;
using XFilter.Shared.Interfaces;

namespace XFilter.Shared.Collections
{
    /// <summary>
    /// Binary stream class.  Note that only streams up to 4gb are supported.
    /// </summary>
    public class BinaryStream : IBinaryStream, IDisposable
    {
        #region BinaryStream private & statics

        private delegate dynamic StreamReadHandler(BinaryReader reader);
        private delegate void StreamWriteHandler(BinaryWriter writer, dynamic value);

        private static readonly Dictionary<Type, StreamReadHandler> Readers =
            new Dictionary<Type, StreamReadHandler>()
            {
                { typeof(SByte), reader => reader.ReadSByte() },
                { typeof(Byte), reader => reader.ReadByte() },
                { typeof(Int16), reader => reader.ReadInt16() },
                { typeof(UInt16), reader => reader.ReadUInt16() },
                { typeof(Int32), reader => reader.ReadInt32() },
                { typeof(UInt32), reader => reader.ReadUInt32() },
                { typeof(Int64), reader => reader.ReadInt64() },
                { typeof(UInt64), reader => reader.ReadUInt64() },
                { typeof(Single), reader => reader.ReadSingle() }, //32-bit single precision
                { typeof(Double), reader => reader.ReadDouble() }, //64-bit double precision

            };

        private static readonly Dictionary<Type, StreamWriteHandler> Writers =
            new Dictionary<Type, StreamWriteHandler>()
            {
                { typeof(SByte), (writer, value) => writer.Write(value) },
                { typeof(Byte), (writer, value) => writer.Write(value) },
                { typeof(Int16), (writer, value) => writer.Write(value) },
                { typeof(UInt16), (writer, value) => writer.Write(value) },
                { typeof(Int32), (writer, value) => writer.Write(value) },
                { typeof(UInt32), (writer, value) => writer.Write(value) },
                { typeof(Int64), (writer, value) => writer.Write(value) },
                { typeof(UInt64), (writer, value) => writer.Write(value) },
                { typeof(Single), (writer, value) => writer.Write(value) }, //32-bit single precision
                { typeof(Double), (writer, value) => writer.Write(value) }, //64-bit double precision
            };

        private MemoryStream _memoryStream;
        private BinaryReader _reader;
        private BinaryWriter _writer;
        private bool _isDisposed;

        #endregion

        #region Constructors / destructor

        /// <summary>
        /// Initializes a new instance of <see cref="BinaryStream"/> class for writing.
        /// </summary>
        public BinaryStream()
        {
            _memoryStream = new MemoryStream();
            _writer = new BinaryWriter(_memoryStream);
            _reader = null;

            this.Mode = BinaryStreamMode.Write;
        }

        /// <summary>
        /// Initializes a new instance of <see cref="BinaryStream"/> for reading (static size).
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="offset"></param>
        /// <param name="count"></param>
        public BinaryStream(byte[] buffer, int offset, int count)
        {
            _memoryStream = new MemoryStream(buffer, offset, count);
            _reader = new BinaryReader(_memoryStream);
            _writer = null;

            this.Mode = BinaryStreamMode.Read;
        }

        /// <summary>
        /// Finalizes a instance of <see cref="BinaryStream"/> class.
        /// </summary>
        ~BinaryStream()
        {
            this.Dispose(false);
        }

        #endregion

        #region IBinaryStream

        /// <summary>
        /// Gets the binary stream mode.
        /// </summary>
        public BinaryStreamMode Mode { get; private set; }

        /// <summary>
        /// Gets the stream length. Don't mess this with capacity.
        /// </summary>
        public int Length => (int)(_memoryStream.Length);

        /// <summary>
        /// Gets the current stream offset.
        /// </summary>
        public int Offset => (int)(_memoryStream.Position);

        /// <summary>
        /// Gets count of remaining bytes to read, only appliable to read mode.
        /// </summary>
        public int RemainRead => (int)(_memoryStream.Length - _memoryStream.Position);

        /// <summary>
        /// Reads a value of given type from the stream.
        /// </summary>
        /// <typeparam name="T">The value type.</typeparam>
        /// <returns></returns>
        public T Read<T>()
        {
            if (Readers.TryGetValue(typeof(T), out StreamReadHandler reader))
                throw new ArgumentException($"Type [{typeof(T).Name}] is not readable.");

            if (this.Mode != BinaryStreamMode.Read)
                throw new InvalidOperationException("Stream is not in read mode.");

            return reader.Invoke(_reader);
        }

        /// <summary>
        /// Reads a collection of values of given type from the stream.
        /// </summary>
        /// <typeparam name="T">The type of values.</typeparam>
        /// <param name="count">The count of items to read.</param>
        /// <returns></returns>
        public ICollection<T> Read<T>(int count)
        {
            if (Readers.TryGetValue(typeof(T), out StreamReadHandler reader))
                throw new ArgumentException($"Type [{typeof(T).Name}] is not readable.");

            if (this.Mode != BinaryStreamMode.Read)
                throw new InvalidOperationException("Stream is not in read mode.");

            ICollection<T> result = new List<T>(count);
            for (int i = 0; i < count; i++)
                result.Add(reader.Invoke(_reader));

            return result;
        }

        /// <summary>
        /// Writes value(s) of given type to the stream.
        /// </summary>
        /// <typeparam name="T">The value type.</typeparam>
        /// <param name="items">The values.</param>
        public void Write<T>(params T[] items)
        {
            if (!Writers.TryGetValue(typeof(T), out StreamWriteHandler writer))
                throw new ArgumentException($"Type [{typeof(T).Name}] is not writable.");

            if (this.Mode != BinaryStreamMode.Write)
                throw new InvalidOperationException("Stream is not in write mode.");

            foreach (var item in items)
                writer.Invoke(_writer, item);
        }

        /// <summary>
        /// Writes collection to the stream (with start offset & count).
        /// </summary>
        /// <typeparam name="T">The value type.</typeparam>
        /// <param name="items">The collection.</param>
        /// <param name="offset">The starting offset.</param>
        /// <param name="count">The count of items to take starting with offset.</param>
        public void WriteArray<T>(T[] items, int offset, int count)
        {
            if (!Writers.TryGetValue(typeof(T), out StreamWriteHandler writer))
                throw new ArgumentException($"Type [{typeof(T).Name}] is not writable.");

            if (offset + count > items.Length)
                throw new IndexOutOfRangeException("Offset / count out of source array bounds.");

            for (int i = offset; i < offset + count; i++)
                writer.Invoke(_writer, items[i]);
        }

        #endregion

        #region IDisposable

        /// <summary>
        /// Implements the IDisposable Dispose() method.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Implements the IDisposable Dispose(bool disposing) method. Disposes the streams.
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (_isDisposed)
                return;

            if (disposing)
            {
                _reader?.Dispose();
                _writer?.Dispose();
                _memoryStream?.Dispose();
            }

            _isDisposed = true;
        }

        #endregion

        #region Misc funcs

        /// <summary>
        /// Gets the buffer (with proper length).
        /// </summary>
        /// <returns></returns>
        public ArraySegment<byte> GetBytes()
        {
            var buffer = _memoryStream.GetBuffer();
            return new ArraySegment<byte>(buffer, 0, (int)this.Length);
        }

        #endregion
    }
}

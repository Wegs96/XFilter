﻿using System.Net;
using System.Net.Sockets;

namespace XFilter.Shared.Extensions
{
    /// <summary>
    /// Socket extension class.
    /// </summary>
    public static class SocketExtensions
    {
        /// <summary>
        /// Closes the socket without throwing any exceptions.
        /// </summary>
        /// <param name="socket">The socket.</param>
        public static void CloseNoThrow(this Socket socket)
        {
            try
            {
                socket.Close();
            }
            catch { }
        }

        /// <summary>
        /// Gets socket IPv4 address string.
        /// </summary>
        /// <param name="socket">The socket.</param>
        /// <returns>null on error.</returns>
        public static string GetAddressString(this Socket socket)
        {
            string result = null;
            try
            {
                result = ((socket.LocalEndPoint ?? socket.RemoteEndPoint) as IPEndPoint).Address.ToString();
            }
            catch { }
            return result;
        }
    }
}

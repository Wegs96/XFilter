﻿namespace XFilter.Shared.Interfaces
{
    /// <summary>
    /// Packet stream interface. At SRO strings are prefixed by 2 bytes length.
    /// </summary>
    public interface IPacketStream : IBinaryStream
    {
        /// <summary>
        /// Gets the packet opcode.
        /// </summary>
        ushort Opcode { get; }

        /// <summary>
        /// Gets the value indicating if the packet is encrypted.
        /// </summary>
        bool IsEncrypted { get; }

        /// <summary>
        /// Gets the value indicating if the packet is massive.
        /// </summary>
        bool IsMassive { get; }

        /// <summary>
        /// Reads a ASCII string with given codepage from the stream.
        /// </summary>
        /// <param name="codepage">The codepage.</param>
        /// <returns>The string.</returns>
        string ReadStringASCII(int codepage = 1251);

        /// <summary>
        /// Writes a ASCII string with given codepage to the stream.
        /// </summary>
        /// <param name="str">The string to write.</param>
        /// <param name="codepage">The string codepage.</param>
        void WriteStringASCII(string str, int codepage = 1251);

        /// <summary>
        /// Reads a unicode string from the stream.
        /// </summary>
        /// <returns>The string.</returns>
        string ReadStringUnicode();

        /// <summary>
        /// Writes a unicode string to the stream.
        /// </summary>
        /// <param name="str">The string.</param>
        void WriteStringUnicode(string str);
    }
}

﻿namespace XFilter.Shared.Network
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Thanks to Daxters SilkroadPacketApi :).
    /// </summary>
    public static class PacketExtensions
    {
        //--------------------------------------------------------------------------
        #region Reader / writer delegates & method defs

        private delegate dynamic PacketReadHandler(XPacket packet);
        private delegate void PacketWriteHandler(XPacket packet, dynamic value);

        /// <summary>
        /// Types that are available for reading as enums.
        /// </summary>
        private static readonly Dictionary<Type, PacketReadHandler> _enumReaders =
            new Dictionary<Type, PacketReadHandler>()
            {
                { typeof(Byte), reader => reader.ReadValue<Byte>() },
                { typeof(SByte), reader => reader.ReadValue<SByte>() },
                { typeof(UInt16), reader => reader.ReadValue<UInt16>() },
                { typeof(Int16), reader => reader.ReadValue<Int16>() },
                { typeof(UInt32), reader => reader.ReadValue<UInt32>() },
                { typeof(Int32), reader => reader.ReadValue<Int32>() },
                { typeof(UInt64), reader => reader.ReadValue<UInt64>() },
                { typeof(Int64), reader=> reader.ReadValue<Int64>() }
            };

        /// <summary>
        /// Types that are available for writing as enums.
        /// </summary>
        private static readonly Dictionary<Type, PacketWriteHandler> _enumWriters =
            new Dictionary<Type, PacketWriteHandler>()
            {
                { typeof(Byte), (packet, value) => packet.WriteValue<Byte>(value) },
                { typeof(SByte), (packet, value) => packet.WriteValue<SByte>(value) },
                { typeof(UInt16), (packet, value) => packet.WriteValue<UInt16>(value) },
                { typeof(Int16), (packet, value) => packet.WriteValue<Int16>(value) },
                { typeof(UInt32), (packet, value) => packet.WriteValue<UInt32>(value) },
                { typeof(Int32), (packet, value) => packet.WriteValue<Int32>(value) },
                { typeof(UInt64), (packet, value) => packet.WriteValue<UInt64>(value) },
                { typeof(Int64), (packet, value) => packet.WriteValue<Int64>(value) }
            };

        #endregion

        //--------------------------------------------------------------------------

        #region Readers

        /// <summary>
        /// Reads enum with given underlying type. See _enumReaders for supported types.
        /// </summary>
        /// <typeparam name="TEnum">The enum type.</typeparam>
        /// <param name="packet">The packet instance.</param>
        /// <returns>Readen value.</returns>
        public static TEnum ReadEnum<TEnum>(this XPacket packet) where
            TEnum : struct, IComparable, IFormattable, IConvertible
        {
            if (!typeof(TEnum).IsEnum)
                throw new ArgumentException("Given type is not enumaration.");

            //TODO: Check the type name output !
            if (!_enumReaders.TryGetValue(Enum.GetUnderlyingType(typeof(TEnum)), out PacketReadHandler reader))
                throw new ArgumentException($"Reader for type {typeof(TEnum).Name} does not exist.");

            return (TEnum)reader.Invoke(packet);
        }

        #endregion

        //--------------------------------------------------------------------------

        #region Writers

        /// <summary>
        /// Writes enum with given underlying type. See _enumWriters for supported types.
        /// </summary>
        /// <typeparam name="TEnum">The enum type.</typeparam>
        /// <param name="packet">The packet instance.</param>
        /// <param name="enum">The enum.</param>
        public static void WriteEnum<TEnum>(this XPacket packet, TEnum @enum) where 
            TEnum : struct, IComparable, IFormattable, IConvertible
        {
            if (!typeof(TEnum).IsEnum)
                throw new ArgumentException($"Given type {typeof(TEnum).Name} is not enumeration.");

            if (!_enumWriters.TryGetValue(Enum.GetUnderlyingType(typeof(TEnum)), out PacketWriteHandler writer))
                throw new ArgumentException($"Writer for type {typeof(TEnum).Name} does not exist.");

            writer(packet, @enum);
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}

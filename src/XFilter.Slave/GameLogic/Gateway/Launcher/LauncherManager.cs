﻿namespace XFilter.Slave.GameLogic.Gateway.Launcher
{
    using DataModel.Protocol;
    using NLog;
    using System.Linq;
    using XFilter.DataModel.Protocol.Gateway;
    using XFilter.Shared.Network;
    using XFilter.Slave.Protocol;
    using XFilter.Slave.Services.Gateway;

    class LauncherManager
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private readonly GatewayService _service;
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public LauncherManager(GatewayService service)
        {
            _service = service;
        }

        private LauncherManager()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region Logic - news

        public bool HandleNewsResponse(GatewayContext context, XPacket packet)
        {
            var entity = new LauncherNewsEntity();
            entity.ReadFromPacket(packet);

            //REMOVEME
            entity.AddArticle(new LauncherNewsItem("hello", "world", true));
            //Logger.Debug("Entity handled.");

            context.Send(PacketSource.Server, entity.WriteToPacket());
            return true;
        }

        #endregion

        //--------------------------------------------------------------------------

        #region Logic - patch

        public bool HandlePatchResponse(GatewayContext context, XPacket packet)
        {
            var entity = new PatchInfoEntity();
            entity.ReadFromPacket(packet);

            //No update required - client is up to date, or error.

            if (entity.Code != PacketResultCode.Success || entity.SubCode != PatchSubCode.Update)
            {
                Logger.Debug("Entity handled.");

                context.Send(PacketSource.Server, entity.WriteToPacket());
                return true;
            }

            //We need to spoof download server address.

            var allRules = gs.Service.GetGatewayByRefID(context.State.RefServiceID).Configuration.RedirectionRules;
            var ruleRec = allRules.Rules.FirstOrDefault(
                item => item.SrcAddr == entity.DownloadServerIp &&
                item.SrcPort == entity.DownloadServerPort);


            if (ruleRec == null)
            {
                Logger.Warn($"No redirection rules for {entity.DownloadServerIp}:{entity.DownloadServerPort}.");
                context.Send(PacketSource.Server, packet);
                return true;
            }


            entity.DownloadServerIp = ruleRec.DestAddr;
            entity.DownloadServerPort = (ushort)ruleRec.DestPort;

            Logger.Debug("Entity handled.");

            context.Send(PacketSource.Server, entity.WriteToPacket());
            return true;
        }

        #endregion


        //--------------------------------------------------------------------------
    }
}

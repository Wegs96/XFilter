﻿using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using XFilter.Shared.Network;

namespace XFilter.Slave.Interfaces
{
    public enum RelayConnectionType
    {
        /// <summary>
        /// Client socket.
        /// </summary>
        Client,
        /// <summary>
        /// Module socket.
        /// </summary>
        Module
    }
    
    public interface IRelayClientContext
    {
        /// <summary>
        /// Starts the relay connection.
        /// </summary>
        /// <param name="clientSocket">The client socket.</param>
        /// <param name="moduleEp">The module address.</param>
        /// <param name="connectionTimeout">The timeout for establishing connection to server module.</param>
        /// <returns></returns>
        bool Start(Socket clientSocket, IPEndPoint moduleEp, int connectionTimeout);


        /// <summary>
        /// Stops the relay connection and fires Disconnected event.
        /// </summary>
        /// <param name="doSessionManagerCallback"><b>Must</b> be set to true if called from session manager.</param>
        void Stop(bool calledFromSessionManager);

        /// <summary>
        /// Starts reciving data from player or module socket.
        /// </summary>
        /// <param name="connectionType"></param>
        /// <returns></returns>
        Task StartReceivingFrom(RelayConnectionType connectionType);

        /// <summary>
        /// Starts sending packet(s) to player or module socket.
        /// </summary>
        /// <param name="connectionType">The target socket.</param>
        /// <param name="packets">The packet(s).</param>
        void StartSendingTo(RelayConnectionType connectionType, params XPacket[] packets);
    }
}

﻿using System.Threading.Tasks;
using XFilter.Shared.Network;
using XFilter.Slave.Services;

namespace XFilter.Slave.Interfaces
{
    /// <summary>
    /// The packet handler interface.
    /// </summary>
    public interface IRelayPacketHandler
    {
        //async
        Task<bool> Execute(RelayClientContextBase context, XPacket packet);
    }
}

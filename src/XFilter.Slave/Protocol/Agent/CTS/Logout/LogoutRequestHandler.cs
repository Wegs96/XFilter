﻿using NLog;
using System.Threading.Tasks;
using XFilter.DataModel.Protocol.Agent;
using XFilter.Shared.Network;
using XFilter.Slave.Interfaces;
using XFilter.Slave.Services;
using XFilter.Slave.Services.Agent;


namespace XFilter.Slave.Protocol.Agent
{
    /// <summary>
    /// C->S CLIENT_AGENT_LOGOUT_REQUEST = 0x7005
    /// TESTME
    /// </summary>
    class LogoutRequestHandler : IRelayPacketHandler
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        public Task<bool> Execute(RelayClientContextBase context, XPacket packet) =>
            this.ExecuteEx(context as AgentContext, packet);

        private Task<bool> ExecuteEx(AgentContext context, XPacket packet)
        {
            var entity = new LogoutRequestEntity();
            entity.ReadFromPacket(packet);

            var svc = gs.Service.GetAgentByRefID(context.RefServiceID);
            var delay = svc.LogoutManager.GetTimeTillNextLogoutPossible(context);

            //User is able to log out.
            if (delay == 0 || !svc.LogoutManager.IsLogoutDelayEnabled)
            {
                context.Send(PacketSource.Client, packet);
                return Task.FromResult<bool>(true);
            }
            else
            {
                var notice = new ChatUpdateEntity(
                    ChatType.Notice,
                    svc.LogoutManager.GetLogoutDelayNoticeMsg(context, delay));
                context.Send(PacketSource.Server, notice.WriteToPacket());
                return Task.FromResult<bool>(true);
            }
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}

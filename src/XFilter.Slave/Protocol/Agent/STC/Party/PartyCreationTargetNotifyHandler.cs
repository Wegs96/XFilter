﻿using NLog;
using System.Threading.Tasks;
using XFilter.Shared.Network;
using XFilter.Slave.Services;
using XFilter.Slave.Services.Agent;

/*
namespace XFilter.DataModel.Protocol.Agent
{
    /// <summary>
    /// S->C SERVER_AGENT_PARTY_CREATE_WITH = 0x3080
    /// Notify the target user about party creation...
    /// TESTME
    /// </summary>
    class PartyCreationTargetNotifyHandler : IRelayPacketHandler
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        public Task<bool> Execute(RelayClientContextBase context, XPacket packet) =>
            this.ExecuteEx(context as AgentContext, packet);

        private Task<bool> ExecuteEx(AgentContext context, XPacket packet)
        {
            var entity = new PartyCreateRequestEntity();
            bool fullyParsed = entity.ReadFromPacket(packet);

            if (!fullyParsed)
            {
                Logger.Debug(XFilter.Shared.Helpers.DebugHelper.HexDump(packet.GetBytes()));
                Logger.Warn("Entity not fully parsed.");
                context.Send(PacketSource.Server, packet);
                return Task.FromResult<bool>(true);
            }

            Logger.Debug("Entity handled.");

            context.Send(PacketSource.Server, entity.WriteToPacket());
            return Task.FromResult<bool>(true);
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
*/
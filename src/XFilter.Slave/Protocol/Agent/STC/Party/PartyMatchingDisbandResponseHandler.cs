﻿using NLog;
using System.Threading.Tasks;
using XFilter.DataModel.Protocol.Agent;
using XFilter.Shared.Network;
using XFilter.Slave.Interfaces;
using XFilter.Slave.Services;
using XFilter.Slave.Services.Agent;

namespace XFilter.Slave.Protocol.Agent
{
    /// <summary>
    /// S->C SERVER_AGENT_PARTY_MATCHING_DISBAND_RESPONSE = 0xB06B
    /// TESTME
    /// </summary>
    class PartyMatchingDisbandResponseHandler : IRelayPacketHandler
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        public Task<bool> Execute(RelayClientContextBase context, XPacket packet) =>
        this.ExecuteEx(context as AgentContext, packet);

        private Task<bool> ExecuteEx(AgentContext context, XPacket packet)
        {
            var entity = new PartyMatchingDisbandResponseEntity();
            bool fullyParsed = entity.ReadFromPacket(packet);

            if (!fullyParsed)
            {
                Logger.Debug(XFilter.Shared.Helpers.DebugHelper.HexDump(packet.GetBytes()));
                Logger.Warn("Entity not fully parsed.");
                context.Send(PacketSource.Server, packet);
                return Task.FromResult<bool>(true);
            }

            Logger.Debug("Entity handled.");

            context.Send(PacketSource.Server, entity.WriteToPacket());
            return Task.FromResult<bool>(true);
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}

﻿using NLog;
using System.Threading.Tasks;
using XFilter.DataModel.Protocol.Gateway;
using XFilter.Shared.Network;
using XFilter.Slave.Interfaces;
using XFilter.Slave.Services;
using XFilter.Slave.Services.Gateway;

namespace XFilter.Slave.Protocol.Gateway
{
    /// <summary>
    /// C->S CLIENT_GATEWAY_SHARD_LIST_PING_REQUEST = 0x6106, Encrypted
    /// TESTME
    /// </summary>
    class ShardListPingRequestHandler : IRelayPacketHandler
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        public Task<bool> Execute(RelayClientContextBase context, XPacket packet) =>
            this.ExecuteEx(context as GatewayContext, packet);

        private Task<bool> ExecuteEx(GatewayContext context, XPacket packet)
        {
            var entity = new ShardListPingRequestEntity();
            entity.ReadFromPacket(packet);

            Logger.Debug("Entity handled.");

            context.Send(PacketSource.Client, packet);
            return Task.FromResult<bool>(true);
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}

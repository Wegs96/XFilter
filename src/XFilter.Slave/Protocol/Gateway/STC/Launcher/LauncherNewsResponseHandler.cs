﻿using NLog;
using System.Threading.Tasks;
using XFilter.Shared.Network;
using XFilter.Slave.Interfaces;
using XFilter.Slave.Services;
using XFilter.Slave.Services.Gateway;


namespace XFilter.Slave.Protocol.Gateway
{
    /// <summary>
    /// S->C SERVER_GATEWAY_LAUNCHER_NEWS_RESPONSE = 0xA104, Massive
    /// TESTME
    /// </summary>
    class LauncherNewsResponseHandler : IRelayPacketHandler
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        public Task<bool> Execute(RelayClientContextBase context, XPacket packet) =>
            this.ExecuteEx(context as GatewayContext, packet);

        private Task<bool> ExecuteEx(GatewayContext context, XPacket packet)
        {
            var service = gs.Service.GetGatewayByRefID(context.State.RefServiceID);
            return Task.FromResult<bool>(
                service.LauncherManager.HandleNewsResponse(context, packet)
                );
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}

﻿using NLog;
using System.Threading.Tasks;
using XFilter.Shared.Network;
using XFilter.Slave.Interfaces;
using XFilter.Slave.Services;

namespace XFilter.Slave.Protocol.Global
{
    /// <summary>
    /// C->S CLIENT_PING_REQUEST = 0x2002
    /// NOTE: Different ExecuteEx call !.
    /// TESTME
    /// </summary>
    class PingRequestHandler : IRelayPacketHandler
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        public Task<bool> Execute(RelayClientContextBase context, XPacket packet) =>
            this.ExecuteEx(context, packet);

        
        private Task<bool> ExecuteEx(RelayClientContextBase context, XPacket packet)
        {
            Logger.Debug("Entity handled.");

            context.Send(PacketSource.Client, packet);
            return Task.FromResult<bool>(true);
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}

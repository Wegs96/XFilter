﻿using System;
using System.Net.Sockets;
using XFilter.Shared.Helpers;
using XFilter.Slave.Protocol;
using XFilter.Slave.Services.Core;
using NLog;
using XFilter.Slave.Services.Core.Relay;

namespace XFilter.Slave.Services.Agent
{
    class AgentContext : RelayClientContext
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private AgentService _service;
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private SplitPacketManager _splitManager;

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        /// <summary>
        /// The agent user context.
        /// </summary>
        /// <param name="client">The client socket.</param>
        /// <param name="service">The service.</param>
        public AgentContext(Socket client, AgentService service) :
            base(client, service, service.ContextManager)
        {
            _service = service;
            _splitManager = new SplitPacketManager(
                _service.PacketDispatcher.ProcessWorkItem);

            this.State = new AgentContextState(
                _service.RefServiceID, NetHelper.GetSockIpAddrStr(client));

            base.OnDataReceived = this.DataReceivedHandler;

            base.PulseActivity();
        }

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties
        
        /// <summary>
        /// The referenced service ID.
        /// </summary>
        public int RefServiceID => _service.RefServiceID;

        /// <summary>
        /// The agent user context state.
        /// </summary>
        public AgentContextState State
        { get; private set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        /// <summary>
        /// Fired once some data is received from client or server.
        /// </summary>
        /// <param name="source">The data source.</param>
        /// <param name="data">The data source.</param>
        /// <param name="count">The count of bytes received.</param>
        private void DataReceivedHandler(PacketSource source, byte[] data, int count)
        {
            //Work queue stuff here
            var securityObj = (source == PacketSource.Client) ? _clientSecurity : _moduleSecurity;
            try
            {
                securityObj.Recv(data, 0, count);

                var packets = securityObj.TransferIncoming();
                if (packets != null)
                {
                    foreach (var packet in packets)
                    {
                        bool isSplit = _splitManager.HandlePacket(packet, this);
                        if(isSplit)
                        {
                            Logger.Debug("Split packet detected 0x{0:X}.", packet.Opcode);
                        }

                        
                        if(!isSplit)
                            _service.PacketDispatcher.ProcessWorkItem(
                                new RelayPacketWorkItem(source, this, packet)
                                );
                    }
                }

                this.FlushToNet(source);
            }
            catch(Exception ex)
            {
                Logger.Warn($"DataReceivedHandler (ssa internal ex?). {ex}");
            }
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}

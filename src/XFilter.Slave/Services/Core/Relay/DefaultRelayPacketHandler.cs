﻿using NLog;
using System.Threading.Tasks;
using XFilter.Shared.Network;
using XFilter.Slave.Interfaces;


namespace XFilter.Slave.Services
{
    internal sealed class DefaultRelayPacketHandler : IRelayPacketHandler
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        public Task<bool> Execute(RelayClientContextBase context, XPacket packet)
        {
            Logger.Warn("Unknown packet. Opcode: 0x{0:X}", packet.Opcode);
            return Task.FromResult<bool>(true);
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}

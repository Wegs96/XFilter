﻿namespace XFilter.Slave.Services
{
    using XFilter.Shared.Collections;
    using XFilter.Shared.Network;
    using XFilter.Slave.Protocol;


    public sealed class RelayPacketWorkItem : WorkItem
    {
        //--------------------------------------------------------------------------

        #region Constructors

        /// <summary>
        /// The ctor.
        /// </summary>
        /// <param name="context">The client context.</param>
        /// <param name="packet">The packet to process</param>
        public RelayPacketWorkItem(PacketSource source, RelayClientContextBase context, XPacket packet) : base()
        {
            this.Source = source;
            this.ClientContext = context;
            this.Packet = packet;
        }

        /// <summary>
        /// The private ctor.
        /// </summary>
        private RelayPacketWorkItem() : base()
        {

        }
        #endregion

        //--------------------------------------------------------------------------

        #region Public properties & bugslayer related

        /// <summary>
        /// The client context.
        /// </summary>
        public readonly RelayClientContextBase ClientContext;

        /// <summary>
        /// The packet that needs to be processed.
        /// </summary>
        public readonly XPacket Packet;

        /// <summary>
        /// The packet source.
        /// </summary>
        public readonly PacketSource Source;

        #endregion

        //--------------------------------------------------------------------------
    }
}

﻿using NLog;
using XFilter.Shared.Helpers;
using XFilter.Shared.Network;
using XFilter.Shared.Interfaces;
using System.Net;
using XFilter.Slave.Interfaces;
using XFilter.Slave.Services.Core.Relay;

namespace XFilter.Slave.Services
{


    /// <summary>
    /// The service base class.
    /// </summary>
    public abstract class ServiceBase<TRelayContext> where TRelayContext : IRelayClientContext
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties

        public ISocketListener SocketListener
        { get; private set; }

        public IContextManager<TRelayContext> ContextManager
        { get; private set; }

        public TimerManager TimerManager
        { get; private set; }

        public int RefServiceID
        { get; protected set; }

        public bool IsRunning
        { get; protected set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public ServiceBase(IPEndPoint bindEp)
        {
            this.TimerManager = new TimerManager();
            this.SocketListener = new SocketListener(bindEp, 200, true);
            this.ContextManager = new RelayContextManager<TRelayContext>();
            this.RefServiceID = 0; 
            this.IsRunning = false;
        }

        protected abstract void SetupPacketHandlers();
        #endregion

        //--------------------------------------------------------------------------
    }
}

﻿namespace XFilter.Slave.Services.Core
{
    using XFilter.DataModel.Protocol;


    class SplitPacketSet
    {
        //--------------------------------------------------------------------------

        #region Public properties

        public GameOpcode StartOpcode
        { get; private set; }

        public GameOpcode DataOpcode
        { get; private set; }

        public GameOpcode EndOpcode
        { get; private set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public SplitPacketSet(GameOpcode startOpcode, GameOpcode dataOpcode, GameOpcode endOpcode)
        {
            this.StartOpcode = startOpcode;
            this.DataOpcode = dataOpcode;
            this.EndOpcode = endOpcode;
        }


        private SplitPacketSet()
        {

        }

        #endregion

        //--------------------------------------------------------------------------
    }
}

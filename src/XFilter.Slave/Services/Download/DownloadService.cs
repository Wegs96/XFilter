﻿namespace XFilter.Slave.Services.Gateway
{
    using NLog;
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using XFilter.DataModel.DTO.Entities;


    internal sealed class DownloadService : ServiceBase
    {
        //--------------------------------------------------------------------------

        #region Private properties & statics

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        //--------------------------------------------------------------------------

        #region Public properties / managers

        public DownloadConfigEntity Configuration
        { get; private set; }

        public RelayPacketDispatcher<DownloadContext> PacketDispatcher
        { get; private set; }

        public DownloadContextManager ContextManager
        { get; private set; }

        #endregion

        //--------------------------------------------------------------------------

        #region Constructors

        public DownloadService()
        {

        }

        #endregion

        //--------------------------------------------------------------------------

        #region Logic

        protected override void SetupPacketHandlers()
        {
            //--------------------------------------------------------------------------

            #region Server -> Client
            #endregion

            //--------------------------------------------------------------------------

            #region Client -> Server
            #endregion

            //--------------------------------------------------------------------------

            Logger.Info($"Handler count: {this.PacketDispatcher.HandlerCount}.");
        }


        /// <summary>
        /// TODO: Verify config
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool Start(DownloadConfigEntity entity)
        {
            //Service manager
            this.Configuration = entity;
            this.PacketDispatcher = new RelayPacketDispatcher<DownloadContext>(this.Configuration.NetEngineConfig);
            this.PacketDispatcher.Initialize();
            this.ContextManager = new DownloadContextManager(this);
            this.ContextManager.Initialize();


            this.RefServiceID = this.Configuration.ServerConfig.ID;

            SetupPacketHandlers();

            var netCfg = this.Configuration.NetEngineConfig;
            bool sockInitialized = base.SocketListener.Initialize(
                netCfg.BindAddress,
                netCfg.BindPort,
                this.ContextManager.EnqueueSessionStartup);

            if (!sockInitialized)
            {
                Logger.Fatal("Could not initialize socket listener. Download service wont run.");
                return false;
            }

            base.SocketListener.AcceptNewClients = true;

            Logger.Debug("Download server is ready for relay service.");

            this.IsRunning = true;
            return true;
        }


        public void Stop()
        {
            Logger.Warn("Download service is shutting down.");

            this.SocketListener.Deinitialize();
            Logger.Warn("Socket listener stopped.");

            this.PacketDispatcher.Deinitialize();
            Logger.Warn("Packet dispatcher deinitialized.");

            this.PacketDispatcher.Deinitialize();
            Logger.Warn("Packet dispatcher deinitialized.");

            Logger.Warn("Gateway service has been shut down.");

            this.IsRunning = false;
        }


        #endregion

        //--------------------------------------------------------------------------
    }
}
